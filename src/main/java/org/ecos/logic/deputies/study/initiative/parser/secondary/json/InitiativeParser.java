package org.ecos.logic.deputies.study.initiative.parser.secondary.json;

import org.ecos.logic.deputies.study.initiative.Initiative;
import org.json.JSONObject;

public class InitiativeParser {
    private InitiativeParser(){}

    public static final class FieldNames {
        private FieldNames(){}
        public static final String LEGISLATURE = "legislature";
        public static final String DOCUMENT = "document";
        public static final String TITLE = "title";
        public static final String LINK = "link";
        public static final String QUALIFICATE_DATE = "qualificateDate";
        public static final String PRESENTED_DATE = "presentedDate";
        public static final String IDENTITY = "identity";
        public static final String AUTHOR = "author";

        public static final class OPTIONALS {
            public static final String RESULT = "result";
        }
    }




    public static Initiative parse(JSONObject jsonObject) {
        return new Initiative(
                jsonObject.optString(FieldNames.LEGISLATURE),
                jsonObject.optString(FieldNames.DOCUMENT),
                jsonObject.optString(FieldNames.TITLE),
                jsonObject.optString(FieldNames.OPTIONALS.RESULT, ""),
                LinkParser.parse(jsonObject.getJSONObject(FieldNames.LINK)),
                jsonObject.optString(FieldNames.QUALIFICATE_DATE),
                jsonObject.optString(FieldNames.PRESENTED_DATE),
                jsonObject.optString(FieldNames.IDENTITY),
                jsonObject.optString(FieldNames.AUTHOR)
        );
    }

}
