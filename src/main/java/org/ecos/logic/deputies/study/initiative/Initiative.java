package org.ecos.logic.deputies.study.initiative;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Initiative {
    private final String legislature;
    private final String document;
    private final String title;
    private final String result;
    private final Link link;
    private final String qualifiedDate;
    private final String presentedDate;
    private final String identity;
    private final String author;

    public Initiative(
            String legislature,
            String document,
            String title,
            String result,
            Link link,
            String qualifiedDate,
            String presentedDate,
            String identity,
            String author) {
        this.legislature = legislature;
        this.document = document;
        this.title = title;
        this.result = result;
        this.link = link;
        this.qualifiedDate = qualifiedDate;
        this.presentedDate = presentedDate;
        this.identity = identity;
        this.author = author;
    }

    public String getLegislature() {
        return legislature;
    }

    public String getDocument() {
        return document;
    }

    public String getTitle() {
        return title;
    }

    public Link getLink() {
        return link;
    }

    public String getQualifiedDate() {
        return qualifiedDate;
    }

    public String getPresentedDate() {
        return presentedDate;
    }

    public String getIdentity() {
        return identity;
    }

    public String getAuthor() {
        return author;
    }

    public String getResult() {
        return result;
    }

    public List<String> getDetailedAuthors() {
        String aux = this.author;
        if (aux.isEmpty())
            return new ArrayList<>();
        aux = aux.replace(" (GS)", "");
        aux = aux.replace(" (GVOX)", "");
        aux = aux.replace(" (GP)", "");
        aux = aux.replace(" (GR)", "");
        aux = aux.replace(" (GEH Bildu)", "");
        aux = aux.replace(" (GCUP-EC-GC)", "");
        aux = aux.replace(" (EAJ-PNV)", "");
        aux = aux.replace(" (Gmx)", "");
        aux = aux.replace(" (GMx)", "");
        aux = aux.replace(" (GPlu)", "");
        aux = aux.replace(" (GCs)", "");
        aux = aux.replace(" (GV)", "");
        aux = aux.replace(" (SGPP)", "");
        aux = aux.replace(" (SGPS)", "");
        aux = aux.replace(" (SGPMX)", "");
        aux = aux.replace(" (SGPERB)", "");
        aux = aux.replace(" (SGPV)", "");
        aux = aux.replace(" (SGPIC)", "");
        aux = aux.replace(" (SGPN)", "");

        aux = aux.replaceAll(" y \\d\\d [Dd]iputados", "");
        aux = aux.replaceAll(" y \\d\\d [Pp]arlamentarios", "");
        aux = aux.replaceAll(" y \\d\\d [Ss]enadores", "");
        aux = aux.replaceAll(" y [Oo]tros?", "");


        aux = aux.trim();
        if (aux.contains("<br />")) {
            return Arrays.stream(aux.split("<br />")).map(String::trim).collect(Collectors.toList());
        }
        return new ArrayList<>(Collections.singletonList(aux));
    }
}
