package org.ecos.logic.deputies.study.initiative;

public class Link {
    private final String documents;
    private final String piece;
    private final String query;
    private final String command;
    private final String fmt;
    private final String formulary;
    private final String url;
    private final String base;

    public Link(String documents, String piece, String query, String command, String fmt, String formulary, String url, String base) {
        this.documents = documents;
        this.piece = piece;
        this.query = query;
        this.command = command;
        this.fmt = fmt;
        this.formulary = formulary;
        this.url = url;
        this.base = base;
    }

    public String getDocuments() {
        return documents;
    }

    public String getPiece() {
        return piece;
    }

    public String getQuery() {
        return query;
    }

    public String getCommand() {
        return command;
    }

    public String getFmt() {
        return fmt;
    }

    public String getFormulary() {
        return formulary;
    }

    public String getUrl() {
        return url;
    }

    public String getBase() {
        return base;
    }
}
