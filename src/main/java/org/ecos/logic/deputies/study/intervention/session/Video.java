package org.ecos.logic.deputies.study.intervention.session;

public class Video {
    private final String timeCodeAsString;
    private final String legislature;
    private final String url;
    private final String linkToDownload;
    private final String linkToBroadcast;
    private final String sequence;

    private final String startCode;
    private final String identity;
    private final String source;

    public Video(String timeCodeAsString, String legislature, String url, String linkToDownload, String linkToBroadcast, String sequence, String startCode, String identity, String source) {
        this.timeCodeAsString = timeCodeAsString;
        this.legislature = legislature;
        this.url = url;
        this.linkToDownload = linkToDownload;
        this.linkToBroadcast = linkToBroadcast;
        this.sequence = sequence;
        this.startCode = startCode;
        this.identity = identity;
        this.source = source;
    }

    public String getTimeCodeAsString() {
        return timeCodeAsString;
    }

    public String getLegislature() {
        return legislature;
    }

    public String getUrl() {
        return url;
    }

    public String getLinkToDownload() {
        return linkToDownload;
    }

    public String getLinkToBroadcast() {
        return linkToBroadcast;
    }

    public String getSequence() {
        return sequence;
    }

    public String getStartCode() {
        return startCode;
    }

    public String getIdentity() {
        return identity;
    }

    public String getSource() {
        return source;
    }

    @Override
    public String toString() {
        return "Video{" +
                "timeCodeAsString='" + timeCodeAsString + '\'' +
                ", legislature='" + legislature + '\'' +
                ", url='" + url + '\'' +
                ", linkToDownload='" + linkToDownload + '\'' +
                ", linkToBroadcast='" + linkToBroadcast + '\'' +
                ", sequence='" + sequence + '\'' +
                ", startCode='" + startCode + '\'' +
                ", identity='" + identity + '\'' +
                ", source='" + source + '\'' +
                '}';
    }
}
