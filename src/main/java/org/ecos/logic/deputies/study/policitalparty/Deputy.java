package org.ecos.logic.deputies.study.policitalparty;

public class Deputy {
    private final int circumscriptionId;
    private final String name;
    private final String surname;
    private final boolean isMale;
    private final int legislatureXivId;

    public Deputy(int circumscriptionId, String name, String surname, boolean isMale, int legislatureXivId) {
        this.circumscriptionId = circumscriptionId;
        this.name = name;
        this.surname = surname;
        this.isMale = isMale;
        this.legislatureXivId = legislatureXivId;
    }

        public int getCircumscriptionId() {
        return circumscriptionId;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public boolean isMale() {
        return isMale;
    }

    public int getLegislatureId() {
        return legislatureXivId;
    }

    public String getSurnameAndName(){
        return String.format("%s, %s",this.surname,this.name);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Deputy deputy = (Deputy) o;

        if (circumscriptionId != deputy.circumscriptionId) return false;
        if (isMale != deputy.isMale) return false;
        if (legislatureXivId != deputy.legislatureXivId) return false;
        if (!name.equals(deputy.name)) return false;
        return surname.equals(deputy.surname);
    }

    @Override
    public int hashCode() {
        int result = circumscriptionId;
        result = 31 * result + name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + (isMale ? 1 : 0);
        result = 31 * result + legislatureXivId;
        return result;
    }
}
