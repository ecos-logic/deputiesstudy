package org.ecos.logic.deputies.study.rest.client;

import org.ecos.logic.deputies.study.intervention.json.InterventionParser;
import org.ecos.logic.deputies.study.intervention.Intervention;
import org.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class InterventionInvocation {
    public List<Intervention> getAllInterventions(int counter) {
        List<Intervention> result = new ArrayList<>();
        for (int i = 1; i <= counter; i++) {
            String s = getInterventionAsStringBy(i);
            result.addAll(InterventionParser.toInterventionCollection(new JSONObject(Objects.requireNonNull(s))));
            System.out.println(s);
        }
        return  result;
    }
    public int getInterventionsCounter() {
        String initiativePageAsString = getInterventionAsStringBy(1);
        System.out.println(initiativePageAsString);
        return InterventionParser.getCounter(new JSONObject(Objects.requireNonNull(initiativePageAsString)));
    }

    private static String getInterventionAsStringBy(int pageNumber) {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();

        RestTemplate restTemplate = restTemplateBuilder.build();

        URI uri = new DefaultUriBuilderFactory().uriString("https://www.congreso.es/es/intervenciones-diputado").
                queryParam("p_p_id","intervenciones").
                queryParam("p_p_lifecycle","2").
                queryParam("p_p_state","normal").
                queryParam("p_p_mode","view").
                queryParam("p_p_resource_id","filtrarListado").
                queryParam("p_p_cacheability","cacheLevelPage").
                queryParam("_intervenciones_legislatura","XIV").
                queryParam("_intervenciones_paginaActual",Integer.toString(pageNumber)).
                build();


        return CommonInvocation.invokePostAndReturnAStringSaveOfException(restTemplate, uri);
    }
}
