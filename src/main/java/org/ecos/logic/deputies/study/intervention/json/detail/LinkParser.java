package org.ecos.logic.deputies.study.intervention.json.detail;

import org.ecos.logic.deputies.study.intervention.detail.Link;
import org.json.JSONObject;

public class LinkParser {
    private LinkParser(){}
    static Link parse(JSONObject jsonObject) {
        return new Link(
            jsonObject.optString("piece"),
            jsonObject.optString("id_documento"),
            jsonObject.optString("formato"),
            jsonObject.optString("query"),
            jsonObject.optString("cmd"),
            jsonObject.optString("url"),
            jsonObject.optString("base"),
            jsonObject.optString("id_iniciativa")
        );
    }

}
