package org.ecos.logic.deputies.study.intervention.json;

import org.ecos.logic.deputies.study.intervention.Link;
import org.json.JSONObject;

public class LinkParser {
    private LinkParser(){}
    static Link parse(JSONObject jsonObject) {
        return new Link(
            jsonObject.optString("piece"),
            jsonObject.optString("docr"),
            jsonObject.optString("formato"),
            jsonObject.optString("query"),
            jsonObject.optString("cmd"),
            jsonObject.optString("url"),
            jsonObject.optString("base")
        );
    }
}
