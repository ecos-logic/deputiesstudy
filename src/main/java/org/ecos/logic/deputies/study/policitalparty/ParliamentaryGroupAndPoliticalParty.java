package org.ecos.logic.deputies.study.policitalparty;

public class ParliamentaryGroupAndPoliticalParty {
    private final String politicalGroupName;
    private final String politicalPartyName;

    public ParliamentaryGroupAndPoliticalParty(String politicalGroupName, String politicalPartyName) {

        this.politicalGroupName = politicalGroupName;
        this.politicalPartyName = politicalPartyName;
    }


    @Override
    public String toString() {
        return "ParliamentaryGroupAndPoliticalParty{" +
                "politicalGroupName='" + politicalGroupName + '\'' +
                ", politicalPartyName='" + politicalPartyName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParliamentaryGroupAndPoliticalParty that = (ParliamentaryGroupAndPoliticalParty) o;

        if (!politicalGroupName.equalsIgnoreCase(that.politicalGroupName)) return false;
        return politicalPartyName.equalsIgnoreCase(that.politicalPartyName);
    }

    @Override
    public int hashCode() {
        int result = politicalGroupName.toLowerCase().hashCode();
        result = 31 * result + politicalPartyName.toLowerCase().hashCode();
        return result;
    }
}
