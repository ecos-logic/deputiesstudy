package org.ecos.logic.deputies.study.initiative.parser.secondary.json;

import org.ecos.logic.deputies.study.initiative.Link;
import org.json.JSONObject;

public class LinkParser {
    private LinkParser(){}
    static Link parse(JSONObject jsonObject) {
        return new Link(
                jsonObject.optString("documents"),
                jsonObject.optString("piece"),
                jsonObject.optString("query"),
                jsonObject.optString("command"),
                jsonObject.optString("fmt"),
                jsonObject.optString("formulary"),
                jsonObject.optString("url"),
                jsonObject.optString("base")
        );
    }

}
