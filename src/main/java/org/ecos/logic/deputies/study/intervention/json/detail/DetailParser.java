package org.ecos.logic.deputies.study.intervention.json.detail;

import org.ecos.logic.deputies.study.intervention.detail.Author;
import org.ecos.logic.deputies.study.intervention.detail.Detail;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailParser {
    private DetailParser(){}
    public static Detail parse(JSONObject jsonObject) {
        List<Author> authorList = new ArrayList<>();
        return new Detail(
            LinkParser.parse(jsonObject.optJSONObject("enlace_expediente",new JSONObject())),
            AuthorParser.parseList(jsonObject.optJSONObject("autores", new JSONObject()),authorList),
            jsonObject.optString("organo"),
            jsonObject.optString("cini"),
            jsonObject.optString("objeto_iniciativa")
        );
    }

}
