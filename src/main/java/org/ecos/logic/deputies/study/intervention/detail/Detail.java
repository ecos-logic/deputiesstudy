package org.ecos.logic.deputies.study.intervention.detail;

import java.util.List;

public class Detail {
    private final Link link;
    private final List<Author> authorList;
    private final String source;
    private final String identity;
    private final String title;

    public Detail(Link link, List<Author> authorList, String source, String identity, String title) {
        this.link = link;
        this.authorList = authorList;
        this.source = source;
        this.identity = identity;
        this.title = title;
    }

    public Link getLink() {
        return link;
    }

    public List<Author> getAuthorList() {
        return authorList;
    }

    public String getSource() {
        return source;
    }

    public String getIdentity() {
        return identity;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Detail{" +
                "link=" + link +
                ", authorList=" + authorList +
                ", source='" + source + '\'' +
                ", identity='" + identity + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
