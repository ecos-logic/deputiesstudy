package org.ecos.logic.deputies.study.intervention.json;

import org.ecos.logic.deputies.study.intervention.Video;
import org.json.JSONObject;

public class VideoParser {
    private VideoParser(){}

    public static Video parse(JSONObject jsonObject) {
        return new Video(
            jsonObject.optString("fin01"),
            jsonObject.optString("legislatura"),
            jsonObject.optString("url01"),
            jsonObject.optString("enlace_descarga"),
            jsonObject.optString("enlace_emision"),
            jsonObject.optString("secuencia"),
            jsonObject.optString("inicio01"),
            jsonObject.optString("id01"),
            jsonObject.optString("organo")
        );
    }

}
