package org.ecos.logic.deputies.study.data.entity;

import javax.persistence.*;

@Entity
@Table(name = "circumscription")
public class Circumscription {
    protected Circumscription() {}
    @Id
    @Column(name = "circumscription_id")



    private Integer id;

    private String name;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Circumscription{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Circumscription that = (Circumscription) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}