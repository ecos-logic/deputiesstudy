package org.ecos.logic.deputies.study.intervention.json.session;

import org.ecos.logic.deputies.study.intervention.session.Video;
import org.json.JSONObject;

import java.util.List;

public class VideoParser {
    private VideoParser(){}
    public static List<Video> parseList(JSONObject jsonObject, List<Video> videoList) {
        videoList.add(parse(jsonObject));

        return videoList;
    }

    public static Video parse(JSONObject jsonObject) {
        return new Video(
            jsonObject.optString("fin01"),
            jsonObject.optString("legislatura"),
            jsonObject.optString("url01"),
            jsonObject.optString("enlace_descarga"),
            jsonObject.optString("enlace_emision"),
            jsonObject.optString("secuencia"),
            jsonObject.optString("inicio01"),
            jsonObject.optString("id01"),
            jsonObject.optString("organo")
        );
    }
}
