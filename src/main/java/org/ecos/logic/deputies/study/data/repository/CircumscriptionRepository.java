package org.ecos.logic.deputies.study.data.repository;

import org.ecos.logic.deputies.study.data.entity.Circumscription;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CircumscriptionRepository extends CrudRepository<Circumscription, Integer> {
    List<Circumscription> findAll();
}
