package org.ecos.logic.deputies.study.deputy.parse.json;

import org.ecos.logic.deputies.study.deputy.Deputy;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DeputyParser {
    private DeputyParser(){}
    public static List<Deputy> parseList(JSONArray jsonArray) {
        List<Deputy> deputyList = new ArrayList<>();
        for (int i=0; i<jsonArray.length();i++) {
            deputyList.add(parse(jsonArray.getJSONObject(i)));
        }
        return deputyList;
    }

    private static Deputy parse(JSONObject jsonObject) {
        return new Deputy(
            jsonObject.optInt("codParlamentario"),
            jsonObject.optString("nombre"),
            jsonObject.optString("apellidos"),
            jsonObject.optString("apellidosNombre"),
            jsonObject.optString("formacion"),
            jsonObject.optString("grupo"),
            jsonObject.optString("fchAlta"),
            jsonObject.optString("fchBaja"),
            jsonObject.optString("genero"),
            jsonObject.optInt("idLegislatura"),
            jsonObject.optInt("idCircunscripcion"),
            jsonObject.optString("nombreCircunscripcion")
        );
    }
}
