package org.ecos.logic.deputies.study.intervention;

public class Link {
    private final String piece;
    private final String documentReference;
    private final String format;
    private final String query;
    private final String command;
    private final String url;
    private final String base;

    public Link(String piece, String documentReference, String format, String query, String command, String url, String base) {
        this.piece = piece;
        this.documentReference = documentReference;
        this.format = format;
        this.query = query;
        this.command = command;
        this.url = url;
        this.base = base;
    }

    public String getPiece() {
        return piece;
    }

    public String getDocumentReference() {
        return documentReference;
    }

    public String getFormat() {
        return format;
    }

    public String getQuery() {
        return query;
    }

    public String getCommand() {
        return command;
    }

    public String getUrl() {
        return url;
    }

    public String getBase() {
        return base;
    }

    @Override
    public String toString() {
        return "Link{" +
                "piece='" + piece + '\'' +
                ", documentReference='" + documentReference + '\'' +
                ", format='" + format + '\'' +
                ", query='" + query + '\'' +
                ", command='" + command + '\'' +
                ", url='" + url + '\'' +
                ", base='" + base + '\'' +
                '}';
    }
}
