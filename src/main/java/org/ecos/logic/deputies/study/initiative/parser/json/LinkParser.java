package org.ecos.logic.deputies.study.initiative.parser.json;

import org.ecos.logic.deputies.study.initiative.Link;
import org.json.JSONObject;

public class LinkParser {
    private LinkParser(){}
    static Link parse(JSONObject jsonObject) {
        return new Link(
            jsonObject.optString("docs"),
            jsonObject.optString("piece"),
            jsonObject.optString("query"),
            jsonObject.optString("cmd"),
            jsonObject.optString("fmt"),
            jsonObject.optString("form1"),
            jsonObject.optString("url"),
            jsonObject.optString("base")
        );
    }

}
