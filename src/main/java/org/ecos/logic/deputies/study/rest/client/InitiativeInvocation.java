package org.ecos.logic.deputies.study.rest.client;

import org.ecos.logic.deputies.study.initiative.Initiative;
import org.ecos.logic.deputies.study.initiative.parser.json.InitiativeParser;
import org.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.ecos.logic.deputies.study.rest.client.CommonInvocation.invokePostAndReturnAStringSaveOfException;

public class InitiativeInvocation {

    public List<Initiative> getAllInitiatives(int counter) {
        List<Initiative> result = new ArrayList<>();
        for (int i = 1; i <= counter; i++) {
            String s = getInitiativeBy(i);
            result.addAll(InitiativeParser.toInitiativeCollection(new JSONObject(Objects.requireNonNull(s))));
            System.out.println(s);
        }
        return  result;
    }

    public int getInitiativesCounter() {
        String initiativePageAsString = getInitiativeBy(1);
        return InitiativeParser.getCounter(new JSONObject(Objects.requireNonNull(initiativePageAsString)));
    }

    private static String getInitiativeBy(int i) {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();

        RestTemplate restTemplate = restTemplateBuilder.build();

        URI uri = new DefaultUriBuilderFactory().uriString("https://www.congreso.es/es/iniciativas-diputado").
                queryParam("p_p_id","iniciativas").
                queryParam("p_p_lifecycle","2").
                queryParam("p_p_state","normal").
                queryParam("p_p_mode","view").
                queryParam("p_p_resource_id","filtrarListado").
                queryParam("p_p_cacheability","cacheLevelPage").
                queryParam("_iniciativas_legislatura","XIV").
                queryParam("_iniciativas_paginaActual",Integer.toString(i)).
                build();

        return invokePostAndReturnAStringSaveOfException(restTemplate, uri);
    }
}
