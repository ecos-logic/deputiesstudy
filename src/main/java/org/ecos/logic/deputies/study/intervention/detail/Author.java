package org.ecos.logic.deputies.study.intervention.detail;


public class Author {
    private final int order;
    private final String name;
    private final int identity;
    private final String legislatureCode;
    private final String link;

    public Author(int order, String name, int identity, String legislatureCode, String link) {
        this.order = order;
        this.name = name;
        this.identity = identity;
        this.legislatureCode = legislatureCode;
        this.link = link;
    }

    public int getOrder() {
        return order;
    }

    public String getName() {
        return name;
    }

    public int getIdentity() {
        return identity;
    }

    public String getLegislatureCode() {
        return legislatureCode;
    }

    public String getLink() {
        return link;
    }

    @Override
    public String toString() {
        return "Author{" +
                "order=" + order +
                ", name='" + name + '\'' +
                ", identity=" + identity +
                ", legislatureCode='" + legislatureCode + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
