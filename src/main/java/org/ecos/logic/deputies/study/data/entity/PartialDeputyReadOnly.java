package org.ecos.logic.deputies.study.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "deputy")
public class PartialDeputyReadOnly {
    protected PartialDeputyReadOnly() {}

    public PartialDeputyReadOnly(String name, String surname, String isMale) {
        this.name = name;
        this.surname = surname;
        this.isMale = isMale;
    }

    @Id
    @Column(name = "deputy_id")
    private Integer id;

    private String name;

    private String surname;

    @Column(name = "is_male")
    private String isMale;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PartialDeputyReadOnly that = (PartialDeputyReadOnly) o;

        if (!name.equals(that.name)) return false;
        if (!surname.equals(that.surname)) return false;
        return isMale.equals(that.isMale);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + isMale.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "PartialDeputyReadOnly{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", isMale='" + isMale + '\'' +
                '}';
    }
}
