package org.ecos.logic.deputies.study.data.repository;

import org.ecos.logic.deputies.study.data.entity.PartialDeputyReadOnly;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DeputyRepository extends CrudRepository<PartialDeputyReadOnly,Integer>{
    List<PartialDeputyReadOnly> findAll();
}
