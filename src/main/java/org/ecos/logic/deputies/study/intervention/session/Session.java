package org.ecos.logic.deputies.study.intervention.session;

import java.util.List;

public class Session {
    private final List<Video> videoLlList;
    private final String name;
    private final String identity;
    private final String phase;

    public Session(List<Video> videoLlList, String name, String identity, String phase) {
        this.videoLlList = videoLlList;
        this.name = name;
        this.identity = identity;
        this.phase = phase;
    }

    public List<Video> getVideoLlList() {
        return videoLlList;
    }

    public String getName() {
        return name;
    }

    public String getIdentity() {
        return identity;
    }

    public String getPhase() {
        return phase;
    }

    @Override
    public String toString() {
        return "Session{" +
                "videoLlList=" + videoLlList +
                ", name='" + name + '\'' +
                ", identity='" + identity + '\'' +
                ", phase='" + phase + '\'' +
                '}';
    }
}
