package org.ecos.logic.deputies.study.intervention;

import org.ecos.logic.deputies.study.intervention.detail.Detail;
import org.ecos.logic.deputies.study.intervention.session.Session;

public class Intervention {
    private final Link link;
    private final String dateAsString;
    private final Detail detail;
    private final String legislatureCode;
    private final String speakerCharge;
    private final String speaker;
    private final Session session;
    private final String documentLink;
    private final String documentOtherLink;
    private final String documentCode;
    private final String documentFormat;
    private final Video video;

    public Intervention(
            Link link,
            String dateAsString,
            Detail detail,
            String legislatureCode,
            String speakerCharge,
            String speaker,
            Session session,
            String documentLink,
            String documentOtherLink,
            String documentCode,
            String documentFormat,
            Video video)
    {
        this.link = link;
        this.dateAsString = dateAsString;
        this.detail = detail;
        this.legislatureCode = legislatureCode;
        this.speakerCharge = speakerCharge;
        this.speaker = speaker;
        this.session = session;
        this.documentLink = documentLink;
        this.documentOtherLink = documentOtherLink;
        this.documentCode = documentCode;
        this.documentFormat = documentFormat;
        this.video = video;
    }

    public Link getLink() {
        return link;
    }

    public String getDateAsString() {
        return dateAsString;
    }

    public Detail getDetail() {
        return detail;
    }

    public String getLegislatureCode() {
        return legislatureCode;
    }

    public String getSpeakerCharge() {
        return speakerCharge;
    }

    public String getSpeaker() {
        return speaker;
    }

    public Session getSession() {
        return session;
    }

    public String getDocumentLink() {
        return documentLink;
    }

    public String getDocumentOtherLink() {
        return documentOtherLink;
    }

    public String getDocumentCode() {
        return documentCode;
    }

    public String getDocumentFormat() {
        return documentFormat;
    }

    public Video getVideo() {
        return video;
    }

    public boolean hasAuthorThatContains(String expression) {
        return this.getDetail().getAuthorList().stream().anyMatch(author -> author.getName().contains(expression));
    }

    public boolean hasSpeakerThatContains(String expression) {
        return this.getSpeaker().contains(expression);
    }

    @Override
    public String toString() {
        return "Intervention{" +
                "link=" + link +
                ", dateAsString='" + dateAsString + '\'' +
                ", detail=" + detail +
                ", legislatureCode='" + legislatureCode + '\'' +
                ", speakerCharge='" + speakerCharge + '\'' +
                ", speaker='" + speaker + '\'' +
                ", session=" + session +
                ", documentLink='" + documentLink + '\'' +
                ", documentOtherLink='" + documentOtherLink + '\'' +
                ", documentCode='" + documentCode + '\'' +
                ", documentFormat='" + documentFormat + '\'' +
                ", video=" + video +
                '}';
    }
}
