package org.ecos.logic.deputies.study.intervention.json;

import org.ecos.logic.deputies.study.intervention.json.detail.DetailParser;
import org.ecos.logic.deputies.study.intervention.json.session.SessionParser;
import org.ecos.logic.deputies.study.intervention.Intervention;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class InterventionParser {
    private InterventionParser(){}
    public static final String KEY_TO_GET_INTERVENTION = "intervencion";
    public static final String FOUNDED_INTERVENTIONS = "intervenciones_encontradas";
    private static final String KEY_TO_NOT_GET_INTERVENTIONS = "intervenciones";

    public static List<Intervention> toInterventionCollection(JSONObject jsonObject) {
        List<Intervention> result = new ArrayList<>();
        for (String keyObj : jsonObject.keySet()) {
            Object valObj = jsonObject.get(keyObj);
            if (valObj instanceof JSONObject) {
                toInterventionCollectionSecondLevel((JSONObject) valObj, result);
            }
        }
        return result;
    }

    public static int getCounter(JSONObject jsonObject) {
        for (String keyObj : jsonObject.keySet()) {
            if (keyObj.startsWith(FOUNDED_INTERVENTIONS)) {
                return jsonObject.getInt(keyObj);
            }
        }
        return 0;
    }

    private static void toInterventionCollectionSecondLevel(JSONObject jsonObject, List<Intervention> interventionList) {
        for (String keyObj : jsonObject.keySet()) {
            if (keyObj.startsWith(KEY_TO_GET_INTERVENTION) && !keyObj.startsWith(KEY_TO_NOT_GET_INTERVENTIONS)) {
                JSONObject jsonObjectSecondLevel = jsonObject.getJSONObject(keyObj);
                interventionList.add(parse(jsonObjectSecondLevel));
            }
        }
    }

    private static Intervention parse(JSONObject jsonObject) {
        return new Intervention(
            LinkParser.parse(jsonObject.optJSONObject("enlace_detalle_intervencion",new JSONObject())),
            jsonObject.optString("fecha"),
            DetailParser.parse(jsonObject.optJSONObject("iniciativa",new JSONObject())),
            jsonObject.optString("legislatura"),
            jsonObject.optString("cargo_orador"),
            jsonObject.optString("orador"),
            SessionParser.parse(jsonObject.optJSONObject("sesion",new JSONObject())),
            jsonObject.optString("edia"),
            jsonObject.optString("pdia"),
            jsonObject.optString("doc"),
            jsonObject.optString("formato_doc"),
            VideoParser.parse(jsonObject.optJSONObject("video_intervencion",new JSONObject()))
        );
    }
}
