package org.ecos.logic.deputies.study.initiative.parser.json;

import org.ecos.logic.deputies.study.initiative.Initiative;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class InitiativeParser {
    private InitiativeParser(){}
    public static final String KEY_TO_GET_INITIATIVE = "iniciativa";
    public static final String KEY_TO_NOT_GET_INITIATIVE = "iniciativas";
    public static final String FOUNDED_INITIATIVES = "iniciativas_encontradas";

    public static final class FieldNames {
        private FieldNames(){}
        public static final String LEGISLATURE = "legislatura";
        public static final String DOCUMENT = "doc";
        public static final String TITLE = "titulo";
        public static final String LINK = "enlace";
        public static final String QUALIFICATE_DATE = "fecha_calificado";
        public static final String PRESENTED_DATE = "fecha_presentado";
        public static final String IDENTITY = "id_iniciativa";
        public static final String AUTHOR = "autor";

        public static final class Optionals {
            private Optionals(){}
            public static final String RESULT = "resultado_tram";
        }
    }

    public static List<Initiative> toInitiativeCollection(JSONObject jsonObject) {
        List<Initiative> result = new ArrayList<>();
        for (String keyObj : jsonObject.keySet()) {
            Object valObj = jsonObject.get(keyObj);
            if (valObj instanceof JSONObject) {
                toInitiativeCollectionSecondLevel((JSONObject) valObj, result);
            }
        }
        return result;
    }

    public static int getCounter(JSONObject jsonObject) {
        for (String keyObj : jsonObject.keySet()) {

            if (keyObj.startsWith(FOUNDED_INITIATIVES)) {
                return jsonObject.getInt(keyObj);
            }
        }
        return 0;
    }

    private static void toInitiativeCollectionSecondLevel(JSONObject jsonObject, List<Initiative> initiativeList) {
        for (String keyObj : jsonObject.keySet()) {
            if (keyObj.startsWith(KEY_TO_GET_INITIATIVE) && !keyObj.startsWith(KEY_TO_NOT_GET_INITIATIVE)) {
                JSONObject jsonObjectSecondLevel = jsonObject.getJSONObject(keyObj);
                initiativeList.add(parse(jsonObjectSecondLevel));
            }
        }
    }

    private static Initiative parse(JSONObject jsonObject) {
        return new Initiative(
                jsonObject.optString(FieldNames.LEGISLATURE),
                jsonObject.optString(FieldNames.DOCUMENT),
                jsonObject.optString(FieldNames.TITLE),
                jsonObject.optString(FieldNames.Optionals.RESULT, ""),
                LinkParser.parse(jsonObject.getJSONObject(FieldNames.LINK)),
                jsonObject.optString(FieldNames.QUALIFICATE_DATE),
                jsonObject.optString(FieldNames.PRESENTED_DATE),
                jsonObject.optString(FieldNames.IDENTITY),
                jsonObject.optString(FieldNames.AUTHOR)
        );
    }

}
