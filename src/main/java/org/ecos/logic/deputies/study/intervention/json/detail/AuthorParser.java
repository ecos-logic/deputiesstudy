package org.ecos.logic.deputies.study.intervention.json.detail;

import org.ecos.logic.deputies.study.intervention.detail.Author;
import org.json.JSONObject;

import java.util.List;

public class AuthorParser {
    private AuthorParser(){}
    static List<Author> parseList(JSONObject jsonObject, List<Author> authorList) {

        for (String keySet:jsonObject.keySet()) {
            authorList.add(parse(jsonObject.optJSONObject(keySet,new JSONObject())));
        }

        return authorList;
    }

    private static Author parse(JSONObject jsonObject) {
        return new Author(
                jsonObject.optInt("orden"),
                jsonObject.optString("nombre"),
                jsonObject.optInt("idDiputado"),
                jsonObject.optString("idLegislatura"),
                jsonObject.optString("enlace")
        );
    }

}
