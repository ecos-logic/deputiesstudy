package org.ecos.logic.deputies.study.rest.client;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

public class CommonInvocation {
    private CommonInvocation(){}
    public static String invokePostAndReturnAStringSaveOfException(RestTemplate restTemplate, URI uri) {
        String resultAsString = null;
        boolean retry;
        do {
            try {
                resultAsString = restTemplate.postForObject(uri.toString(), null, String.class);
                retry = false;
            } catch (RestClientException e) {
                retry = true;
            }
        }while (retry);
        return resultAsString;
    }

}
