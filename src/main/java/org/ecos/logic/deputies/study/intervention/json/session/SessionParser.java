package org.ecos.logic.deputies.study.intervention.json.session;

import org.ecos.logic.deputies.study.intervention.session.Session;
import org.ecos.logic.deputies.study.intervention.session.Video;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SessionParser {
    private SessionParser(){}
    public static Session parse(JSONObject jsonObject) {
        List<Video> videoList = new ArrayList<>();
        return new Session(
            VideoParser.parseList(jsonObject.optJSONObject("videos_fase",new JSONObject()),videoList),
            jsonObject.optString("nombre_sesion"),
            jsonObject.optString("idsesion"),
            jsonObject.optString("fase1")
        );
    }

}
