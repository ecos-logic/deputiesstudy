package org.ecos.logic.deputies.study.policitalparty;

public class ParliamentaryGroup {
    private final String politicalGroupName;

    public ParliamentaryGroup(String politicalGroupName) {
        this.politicalGroupName = politicalGroupName;
    }
    public String getPoliticalGroupName() {
        return politicalGroupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParliamentaryGroup that = (ParliamentaryGroup) o;

        return politicalGroupName.equals(that.politicalGroupName);
    }

    @Override
    public int hashCode() {
        return politicalGroupName.hashCode();
    }

    @Override
    public String toString() {
        return "ParliamentaryGroup{" +
                "politicalGroupName='" + politicalGroupName + '\'' +
                '}';
    }
}
