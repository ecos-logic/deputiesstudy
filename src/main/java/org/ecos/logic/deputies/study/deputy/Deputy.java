package org.ecos.logic.deputies.study.deputy;

public class Deputy {
    private final int identity;
    private final String name;
    private final String surname;
    private final String surnameName;
    private final String politicalParty;
    private final String detailedPoliticalParty;
    private final String start;
    private final String end;
    private final String genre;
    private final int legislatureIdentity;
    private final int locationIdentity;
    private final String locationName;

    public Deputy(
            int identity,
            String name,
            String surname,
            String surnameName,
            String politicalParty,
            String detailedPoliticalParty,
            String start,
            String end,
            String genre,
            int legislatureIdentity,
            int locationIdentity,
            String locationName
    ) {
        this.identity = identity;
        this.name = name;
        this.surname = surname;
        this.surnameName = surnameName;
        this.politicalParty = politicalParty;
        this.detailedPoliticalParty = detailedPoliticalParty;
        this.start = start;
        this.end = end;
        this.genre = genre;
        this.legislatureIdentity = legislatureIdentity;
        this.locationIdentity = locationIdentity;
        this.locationName = locationName;
    }

    public int getIdentity() {
        return identity;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getSurnameName() {
        return surnameName;
    }

    public String getPoliticalParty() {
        return politicalParty;
    }

    public String getDetailedPoliticalParty() {
        return detailedPoliticalParty;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public String getGenre() {
        return genre;
    }

    public int getLegislatureIdentity() {
        return legislatureIdentity;
    }

    public int getLocationIdentity() {
        return locationIdentity;
    }

    public String getLocationName() {
        return locationName;
    }

    public String getSurnameAndName(){
        return String.format("%s, %s",this.surname,this.name);
    }

    @Override
    public String toString() {
        return "Deputy{" +
                "identity=" + identity +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", surnameName='" + surnameName + '\'' +
                ", politicalParty='" + politicalParty + '\'' +
                ", detailedPoliticalParty='" + detailedPoliticalParty + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", genre='" + genre + '\'' +
                ", legislatureIdentity=" + legislatureIdentity +
                ", locationIdentity=" + locationIdentity +
                ", locationName='" + locationName + '\'' +
                '}';
    }
}
