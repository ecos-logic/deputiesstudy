UPDATE
    deputy
        join
    political_party_rel_deputy
        on deputy.deputy_id = political_party_rel_deputy.deputy_id and political_party_rel_deputy.end_date is null
SET
    deputy.active_political_party_rel_deputy_id = political_party_rel_deputy.political_party_rel_deputy_id;