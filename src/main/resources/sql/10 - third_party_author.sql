INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        1,
        'Gobierno '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        2,
        'Comunitat Valenciana - Les Corts '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        3,
        'Comunidad Autónoma de las Illes Balears-Parlamento '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        4,
        'Comunidad Autónoma del Principado de Asturias-Junta General '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        5,
        'Ponencia del Proyecto de Ley reguladora de la protección de las personas que informen sobre infracciones normativas y de lucha contra la corrupción '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        6,
        'Congreso de los Diputados. Pleno '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        7,
        'Comisión de Asuntos Económicos y Transformación Digital '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        8,
        'Grupo Parlamentario Vasco '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        9,
        'Comisión de Igualdad '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        10,
        'Comisión de Justicia '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        11,
        'Senado '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        12,
        'Quevedo Iturbe, Pedro '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        13,
        'Comunidad Autónoma de la Región de Murcia - Asamblea Regional '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        14,
        'Comunidad Autónoma de Cantabria - Parlamento '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        15,
        'Comunidad Autónoma de Andalucía-Parlamento '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        16,
        'Comunidad Autónoma de Cataluña - Parlamento '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        17,
        'Comunidad de Madrid - Asamblea '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        18,
        'Comunidad Autónoma de Canarias - Parlamento '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        19,
        'Comunidad Autónoma de Aragón-Cortes '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        20,
        'Comunidad Autónoma de Galicia - Parlamento '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        21,
        'Comunidad Autónoma del País Vasco - Parlamento '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        22,
        'Don Alejandro Escribano Sanmartín '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        23,
        'Don Miguel Arroyo Pérez '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        24,
        'Doña Soledad Camacho Alcántara '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        25,
        'Comunidad Foral de Navarra - Parlamento '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        26,
        'Don Rafael Folch Machado '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        27,
        'Don Alfonso Galdón Ruiz '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        28,
        'Doña María Begoña Montero Medina '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        29,
        'Comunidad Autónoma de La Rioja - Parlamento '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        30,
        'Don Rafael María López Heredia '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        31,
        'Don Jorge Mira Frías '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        32,
        'Don Ignacio Vargas Pineda '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        33,
        'Don Eduardo Rubio Tolosa '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        34,
        'Don Miguel Ángel González Serradilla '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        35,
        'Don Sergio Luis Sánchez Suárez '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        36,
        'Don Javier Carames Sánchez '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        37,
        'Don Tomás Vizcaíno Recio '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        38,
        'Doña María Gloria Lago Cuadrado '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        39,
        'Doña María Teresa Vicente Giménez '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        40,
        'Don Augustin Marie Ndour Ndong '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        41,
        'Doña Josefa Burón Flores '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        42,
        'Don José Campos Trujillo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        43,
        'Don José Miguel Martín Rodríguez. '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        44,
        'Don Nicolás Martos García de Veas '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        45,
        'Don José Damián Caballero Martínez '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        46,
        'Doña Esmeralda Gómez López '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        47,
        'Don Natan Espinosa Pérez '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        48,
        'Don Manuel Cascos Fernández '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        49,
        'Don José Miguel Martín Rodríguez '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        50,
        'Don Luis Cayo Pérez Bueno '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        51,
        'Olona Choclán, Macarena '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        52,
        'España Reina, Carolina '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        53,
        'García-Pelayo Jurado, María José '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        54,
        'García Egea, Teodoro '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        55,
        'Casado Blanco, Pablo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        56,
        'Jiménez-Becerril Barrio, María Teresa '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        57,
        'Borràs Castanyer, Laura '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        58,
        'Fernández-Roca Suárez, Carlos Hugo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        59,
        'Montesinos Aguayo, Pablo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        60,
        'De Quinto Romero, Marcos '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        61,
        'Arangüena Fernández, Pablo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        62,
        'Muñoz Arbona, David Juan '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        63,
        'Márquez Sánchez, Francisco Javier '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        64,
        'Casero Ávila, Alberto '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        65,
        'Rodríguez Rodríguez, Alberto '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        66,
        'Rosa Torner, Fernando De '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        67,
        'Elorza González, Odón '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        68,
        'Tomás Olivares, Violante '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        69,
        'Rodríguez Calleja, Patricia '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        70,
        'Ramallo Vázquez, María Pilar '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        71,
        'Rojo Noguera, Pilar Milagros '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        72,
        'Garriga Vaz de Concicao, Ignacio '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        73,
        'Lacalle Lacalle, Francisco Javier '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        74,
        'Godoy Tena, María Ascensión '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        75,
        'Martín Rodríguez, Jesús '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        76,
        'Villacampa Arilla, Rubén Marcos '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        77,
        'Casanova Baragaño, Guadalupe María '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        78,
        'Silván Rodríguez, Antonio '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        79,
        'Uribe-Etxebarria Apalategui, Luis Jesús '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        80,
        'Salanueva Murguialday, Amelia '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        81,
        'Martínez Rodríguez, Antonio '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        82,
        'Santos González, Jaime Miguel de los '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        83,
        'Ramos Acosta, Sergio '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        84,
        'Antona Gómez, Asier '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        85,
        'Aragón Ariza, Francisco Javier '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        86,
        'Morales Quesada, Ramón '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        87,
        'Martínez Urionabarrenetxea, Koldo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        88,
        'Fuentes Curbelo, Juan Bernardo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        89,
        'Vidal Matas, Vicenç '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        90,
        'Bueno Alonso, Josefina Antonia '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        91,
        'Vázquez Bermúdez, Miguel Ángel '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        92,
        'Mediavilla Pérez, Rodrigo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        93,
        'Márquez Guerrero, María '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        94,
        'Cleries I Gonzàlez, Josep Lluís '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        95,
        'Picornell Grenzner, Bernat '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        96,
        'Fernández Rubiño, Eduardo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        97,
        'Bailac Ardanuy, Sara '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        98,
        'Serrano Sierra, Rosa María '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        99,
        'Cepeda García De León, José Carmelo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        100,
        'Picó Garcés, Maria Josep '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        101,
        'Suárez Illana, Adolfo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        102,
        'Bustamante Martín, Miguel Ángel '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        103,
        'Valerio Cordero, Magdalena '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        104,
        'Delgado Ramos, Juan Antonio '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        105,
        'González Ramos, Manuel Gabriel '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        106,
        'Cancela Rodríguez, Pilar '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        107,
        'Beltrán de Heredia Arroniz, Estefanía '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        108,
        'Nuet Pujals, Joan Josep '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        109,
        'Martín Llaguno, Marta '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        110,
        'Carcedo Roces, María Luisa '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        111,
        'Alonso-Cuevillas i Sayrol, Jaume '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        112,
        'García Tejerina, Isabel '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        113,
        'Ortega Otero, Marina '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        114,
        'Montilla Martos, José Antonio '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        115,
        'Illueca Ballester, Héctor '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        116,
        'Comisión de Trabajo, Inclusión, Seguridad Social y Migraciones '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        117,
        'Diputación Permanente '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        118,
        'Comisión de Seguimiento y Evaluación de los Acuerdos Pacto de Toledo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        119,
        'Comisión Mixta para la Unión Europea '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        120,
        'Comisión para la Reconstrucción Social y Económica '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        121,
        'Senado - Grupo Parlamentario Popular en el Senado '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        122,
        'Senado - Grupo Parlamentario Vasco en el Senado '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        123,
        'Senado - Grupo Parlamentario Esquerra Republicana - EH Bildu '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        124,
        'Clavijo Batlle, Fernando '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        125,
        'Chinea Correa, Fabián '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        126,
        'Comisión de Industria, Comercio y Turismo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        127,
        'Comisión de Defensa '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        128,
        'Comisión de Transportes, Movilidad y Agenda Urbana '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        129,
        'Comisión de segto. y eval. Acuerdos Pacto de Estado Violencia Género '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        130,
        'Defensor del Pueblo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        131,
        'Comisión Derechos Sociales y Políticas Integrales de la Discapacidad '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        132,
        'Fiscal General del Estado '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        133,
        'Comisión sobre Seguridad Vial '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        134,
        'Comisión Mixta para las Relaciones con el Tribunal de Cuentas '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        135,
        'Comisión de Ciencia, Innovación y Universidades '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        136,
        'Comisión de Agricultura, Pesca y Alimentación '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        137,
        'Comisión de Hacienda y Función Pública '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        138,
        'Comisión de Transición Ecológica y Reto Demográfico '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        139,
        'Consejo de Transparencia y Buen Gobierno. Sr. Presidente '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        140,
        'Agencia Española de Protección de Datos. Sra. Directora '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        141,
        'Comisión de Sanidad y Consumo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        142,
        'Comisión de Cultura y Deporte '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        143,
        'Comisión de Invest. gestión de vacunas y Plan de Vacunación en España '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        144,
        'Comisión Constitucional '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        145,
        'Comisión de Investigación relativa a la utilización ilegal de efectivos, medios y recursos del Ministerio del Interior, con la finalidad de favorecer intereses políticos del PP y de anular pruebas inculpatorias para este partido en casos de corrupción, durante los mandatos de Gobierno del Partido Popular '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        146,
        'Comisión Mixta de Seguridad Nacional '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        147,
        'Comisión de Educación y Formación Profesional '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        148,
        'Comisión de Cooperación Internacional para el Desarrollo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        149,
        'Comisión calidad democrática, contra corrupción y reform. inst. y leg. '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        150,
        'Corporación RTVE. Sr. Presidente '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        151,
        'Comisión Mixta para Coordinación y Seguimiento Estrategia Española ODS '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        152,
        'Consejo de Seguridad Nuclear '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        153,
        'Corporación RTVE '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        154,
        'Comisión Mixta Control Parlam. de la Corporación RTVE y sus Sociedades '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        155,
        'Argüeso Torres, Emilio '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        156,
        'Goñi Sarries, Ruth '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        157,
        'Cortès Gès, Mirella '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        158,
        'Senado - Grupo Parlamentario Democrático (Ciudadanos, Agrupación De Electores \'Teruel Existe\' y Partido Regionalista de Cantabria) '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        159,
        '(152/5) Com.Investigación actuaciones Mº Interior durante gobierno PP '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        160,
        'Comisión de Derechos de la Infancia y Adolescencia '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        161,
        'Comisión de Asuntos Exteriores '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        162,
        'Comisión de Investigación accidente del vuelo JK 5022 de Spanair '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        163,
        'Diputación Permanente de la XIII Legislatura '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        164,
        'Comisión Nacional de los Mercados y la Competencia. Sr. Secretario del Consejo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        165,
        'Comisión Nacional de los Mercados y la Competencia. Sra. Presidenta '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        166,
        'Comisión Nacional del Mercado de Valores. '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        167,
        'Comisión Nacional de los Mercados y la Competencia. Sr. Presidente '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        168,
        'Observatorio de la vida militar. Sr. Presidente '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        169,
        'Banco de España '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        170,
        'Observatorio de la Vida Militar. Sr. Presidente '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        171,
        'Senado - Grupo Parlamentario Socialista '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        172,
        'Senado - Grupo Parlamentario de Izquierda Confederal (Més per Mallorca, Más Madrid, Compromís, Geroa Bai y Agrupación Socialista Gomera) '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        173,
        'Senado - Grupo Parlamentario Izquierda Confederal (Adelante Andalucía, Mès per Mallorca, Más Madrid, Compromís, Geroa Bai y Catalunya en Comú Podem) '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        174,
        'Senado-Mesa '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        175,
        'Comisión del Estatuto de los Diputados '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        176,
        'Congreso de los Diputados. Mesa '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        177,
        'Comunidad Autónoma del País Vasco - Gobierno '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        178,
        'Gobierno de Aragón. Sra. Consejera de Presidencia y Relaciones Institucionales '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        179,
        'Comunidad Autónoma de Castilla y León - Cortes '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        180,
        'Congreso de los Diputados. Sra. Presidenta '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        181,
        'Congreso de los Diputados y Senado en reunión conjunta. Mesas '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        182,
        'Congreso de Diputados y Senado en reunión conjunta. Sres. Presidentes '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        183,
        'Congreso y Senado. Mesas '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        184,
        'Congreso de Diputados y Senado en reunión conjunta. Sras. Presidentas '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        185,
        'Tribunal Constitucional '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        186,
        'Fiscalía General del Estado '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        187,
        'Consejo General del Poder Judicial '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        188,
        'Tribunal Supremo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        189,
        'Tribunal de Cuentas '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        190,
        'Comisión Mixta de Relaciones con el Defensor del Pueblo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        191,
        'Congreso de los Diputados y Senado. Sres. Presidentes '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        192,
        'Ministerio de Defensa. Sra. Ministra '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        193,
        'Comisión Europea. Sr. Vicepresidente '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        194,
        'Martínez de Tejada Pérez, Carlos '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        195,
        'Roselló Bouso, Jorge '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        196,
        'Martínez Salmerón, Joaquín '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        197,
        'Ruiz López, Jesús '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        198,
        'Vera Ruíz-Herrera, Noelia '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        199,
        'Iglesias Turrión, Pablo '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        200,
        'Celaá Diéguez, Isabel '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        201,
        'Marrodán Funes, María '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        202,
        'Corredor Sierra, María Beatriz '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        203,
        'Ribera Rodríguez, Teresa '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        204,
        'Máñez Rodríguez, Elena '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        205,
        'Delgado García, Dolores '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        206,
        'Saura García, Pedro '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        207,
        'Guirao Cabrera, José '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        208,
        'Polo Llavata, Francisco '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        209,
        'Grande-Marlaska Gómez, Fernando '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        210,
        'Planas Puchades, Luis '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        211,
        'Maroto Illera, Reyes '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        212,
        'Izquierdo Roncero, José Javier '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        213,
        'Franco Pardo, José Manuel '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        214,
        'Duque Duque, Pedro '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        215,
        'Campo Moreno, Juan Carlos '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        216,
        'Robles Fernández, Margarita '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        217,
        'Rosell Aguilar, María Victoria '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        218,
        'González Robles, José Miguel '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        219,
        'Salazar Rodríguez, Francisco José '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        220,
        'Cortes Generales. Sr. Letrado Mayor '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        221,
        'Congreso de los Diputados y Senado. Mesas '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        222,
        'Congreso de los Diputados. Sr. Secretario General '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        223,
        'Congreso de los Diputados. Sr. Letrado Mayor '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        224,
        'Senado. Sr. Letrado Mayor '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        225,
        'Congreso de los Diputados y Senado. Sras. Presidentas'
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        666,
        'S.M. El Rey Don Felipe VI '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        226,
        'El Jefe de la Casa de S. M. El Rey '
    );

INSERT INTO
    third_party_author
(
    third_party_author_id,
    name
)
VALUES
    (
        227,
        'Jefe de la Casa de S. M. El Rey '
    );
