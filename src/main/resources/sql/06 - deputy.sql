INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'José Luis',
        'Ábalos Meco',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Santiago',
        'Abascal Conde',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '40',
        'José Luis',
        'Aceves Galindo',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '20',
        'Joseba Andoni',
        'Agirretxea Urresti',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Juan José',
        'Aizcorbe Torra',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '20',
        'Mertxe',
        'Aizpurua Arzallus',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '23',
        'Francisco José',
        'Alcaraz Martos',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '24',
        'Javier',
        'Alfonso Cendón',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '3',
        'Agustín',
        'Almodóbar Barceló',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '47',
        'José Ángel',
        'Alonso Pérez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '36',
        'María Olga',
        'Alonso Suárez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Cayetana',
        'Álvarez de Toledo Peralta-Ramos',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '48',
        'Beatriz',
        'Álvarez Fanjul',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Gerard',
        'Álvarez i García',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Carmen',
        'Andrés Añón',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Josefa',
        'Andrés Barea',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Omar',
        'Anguita Pérez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '6',
        'María Teresa',
        'Angulo Romero',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '42',
        'Javier',
        'Antón Cacho',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Carlos',
        'Aragonés Mendiguchía',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Francisco',
        'Aranda Vargas',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '11',
        'Gemma',
        'Araujo Morales',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '5',
        'Manuel',
        'Arribas Maroto',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Inés',
        'Arrimadas García',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '12',
        'Alberto',
        'Asarta Cuevas',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Jaume',
        'Asens Llodrà',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '3',
        'Lázaro',
        'Azorín Salar',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Edmundo',
        'Bal Francés',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Joan',
        'Baldoví Roda',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '30',
        'Carmen',
        'Baños Ruiz',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '20',
        'Íñigo',
        'Barandiaran Benito',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '36',
        'Javier',
        'Bas Corugeira',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '17',
        'Montserrat',
        'Bassa Coll',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Meritxell',
        'Batet Lamaña',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '43',
        'Ferran',
        'Bel Accensi',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '31',
        'Ione',
        'Belarra Urteaga',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Ana María',
        'Beltrán Villalba',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '23',
        'Laura',
        'Berja Vega',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '37',
        'José Antonio',
        'Bermúdez de Castro Fernández',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Vicente',
        'Betoret Coll',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '3',
        'Patricia',
        'Blanquer Alcaraz',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Genís',
        'Boadella Esteve',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Mireia',
        'Borrás Pabón',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '30',
        'Isabel María',
        'Borrego Cortés',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Ana María',
        'Botella Gómez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Albert',
        'Botran Pahissa',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '11',
        'Eva',
        'Bravo Barco',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '41',
        'Eva Patricia',
        'Bueno Campanario',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '11',
        'José Luis',
        'Bueno Pinto',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '47',
        'Helena',
        'Caballero Gutiérrez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '42',
        'Tomás',
        'Cabezón Casas',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '13',
        'Juan Antonio',
        'Callejas Cano',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Pilar',
        'Calvo Gómez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '24',
        'Pablo Juan',
        'Calvo Liste',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Carmen',
        'Calvo Poyato',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '41',
        'Pablo',
        'Cambronero Piqueras',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '16',
        'Mariana de Gracia',
        'Canales Duque',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Zaida',
        'Cantera de Castro',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '25',
        'Concep',
        'Cañadell Salvia',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '45',
        'Inés María',
        'Cañizares Pacheco',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Joan',
        'Capdevila i Esteve',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '47',
        'Eduardo',
        'Carazo Hermoso',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '41',
        'Beatriz Micaela',
        'Carrillo de los Reyes',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'María',
        'Carvalho Dantas',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '39',
        'Pedro',
        'Casares Hontañón',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '4',
        'Miguel Ángel',
        'Castellón Rubio',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '39',
        'Elena',
        'Castillo López',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '31',
        'Santos',
        'Cerdán León',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '27',
        'Javier',
        'Cerqueiro González',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '13',
        'Ricardo',
        'Chamorro Delmo',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '12',
        'Óscar',
        'Clavell López',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '36',
        'Juan Manuel',
        'Constenla Carbón',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '41',
        'Francisco José',
        'Contreras Peláez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '29',
        'Mario',
        'Cortés Carballo',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '43',
        'Ismael',
        'Cortés Gómez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '14',
        'Rafaela',
        'Crespín Rubio',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '41',
        'María Soledad',
        'Cruz-Guzmán García',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '26',
        'Juan',
        'Cuatrecasas Asua',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '7',
        'Patricia',
        'De las Heras Fernández',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Llanos',
        'De Luna Tobarra',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '4',
        'Rocío',
        'De Meer Méndez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '47',
        'Julio',
        'Del Valle de Iscar',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '39',
        'Emilio Jesús',
        'Del Valle Rodríguez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '32',
        'Celso Luis',
        'Delgado Arce',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '29',
        'Guillermo',
        'Díaz Gómez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '36',
        'Yolanda',
        'Díaz Pérez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '2',
        'José Carlos',
        'Díaz Rodríguez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '35',
        'Luc Andre',
        'Diouf Dioh',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '35',
        'José Francisco',
        'Duque Morán',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '29',
        'José Carlos',
        'Durán Peralta',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '19',
        'José Ignacio',
        'Echániz Salgado',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '50',
        'Pablo',
        'Echenique Robba',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'María Gloria',
        'Elizo Serrano',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Gabriel',
        'Elorriaga Pisarik',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '25',
        'Francesc Xavier',
        'Eritja Ciuró',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Íñigo',
        'Errejón Galván',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'José María',
        'Espejo-Saavedra Conesa',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Iván',
        'Espinosa de los Monteros de Simón',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '48',
        'Aitor',
        'Esteban Bravo',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Cristina Alicia',
        'Esteban Calonje',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '49',
        'Antidio',
        'Fagúndez Campo',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '21',
        'María Luisa',
        'Faneca López',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '24',
        'Andrea',
        'Fernández Benéitez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '10',
        'Ana Belén',
        'Fernández Casero',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '33',
        'Sofía',
        'Fernández Castañón',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '50',
        'Pedro',
        'Fernández Hernández',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '35',
        'María',
        'Fernández Pérez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '21',
        'Tomás',
        'Fernández Ríos',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '2',
        'Rafael',
        'Fernández-Lomana Gutiérrez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '4',
        'Sonia',
        'Ferrer Tesoro',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '33',
        'José María',
        'Figaredo Álvarez-Sala',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '41',
        'Isabel',
        'Franco Carmona',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '36',
        'Diego',
        'Gago Bugarín',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '26',
        'Concepción',
        'Gamarra Ruiz-Clavijo',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Óscar',
        'Gamazo Micó',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '22',
        'Mario',
        'Garcés Sanagustín',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '31',
        'Carlos',
        'García Adanero',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '15',
        'María Montserrat',
        'García Chavarría',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '27',
        'Joaquín María',
        'García Díez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '6',
        'Valentín',
        'García Gómez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '20',
        'María Luisa',
        'García Gurrutxaga',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '6',
        'Maribel',
        'García López',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '33',
        'Roberto',
        'García Morís',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '41',
        'Ezequiel',
        'García Nieto',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'María del Mar',
        'García Puig',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '5',
        'Alicia',
        'García Rodríguez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '20',
        'Pilar',
        'Garrido Gutiérrez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '29',
        'Alberto',
        'Garzón Espinosa',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '33',
        'Paloma',
        'Gázquez Collado',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '30',
        'Luis',
        'Gestoso de Miguel',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Ignacio',
        'Gil Lázaro',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Sara',
        'Giménez Giménez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '38',
        'Héctor',
        'Gómez Hernández',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '15',
        'Antonio',
        'Gómez-Reino Varela',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '13',
        'Miguel Ángel',
        'González Caballero',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '37',
        'Víctor',
        'González Coello de Portugal',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '24',
        'María del Carmen',
        'González Guinda',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '15',
        'Natividad',
        'González Laso',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '29',
        'Ángel Luis',
        'González Muñoz',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '35',
        'Ariagona',
        'González Pérez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Antonio',
        'González Terol',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '15',
        'Marta',
        'González Vázquez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '48',
        'Josune',
        'Gorospe Elezcano',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '25',
        'Inés',
        'Granollers Cunillera',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '43',
        'Sandra',
        'Guaita Esteruelas',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Sonia',
        'Guerra López',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '48',
        'María',
        'Guijarro Ceballos',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '3',
        'Txema',
        'Guijarro García',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Lídia',
        'Guinart Moreno',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '44',
        'Tomás',
        'Guitarte Gimeno',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '52',
        'Fernando Adolfo',
        'Gutiérrez Díaz de Otazu',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '45',
        'Sergio',
        'Gutiérrez Prieto',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '4',
        'Indalecio',
        'Gutiérrez Salinas',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Miguel Ángel',
        'Gutiérrez Vivas',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '7',
        'Sofía',
        'Hernanz Costa',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '44',
        'José Alberto',
        'Herrero Bono',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '18',
        'Pablo',
        'Hispán Iglesias de Ussel',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '18',
        'Pedro Antonio',
        'Honrubia Hurtado',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Belén',
        'Hoyo Juliá',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '14',
        'Antonio',
        'Hurtado Zurera',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '17',
        'Mariona',
        'Illamola Dausà',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '20',
        'Jon',
        'Iñarritu García',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '4',
        'Mercedes',
        'Jara Moreno',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '7',
        'Miguel Ángel',
        'Jerez Juan',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '16',
        'Beatriz',
        'Jiménez Linuesa',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '40',
        'Rodrigo',
        'Jiménez Revuelta',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Rafael',
        'José Vélez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '7',
        'Antonia',
        'Jover Díaz',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '17',
        'Marc',
        'Lamuà Estañol',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '33',
        'Adriana',
        'Lastra Fernández',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Isaura',
        'Leal Fernández',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '38',
        'Sebastián Jesús',
        'Ledesma Martín',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '1',
        'Mikel',
        'Legarda Uriarte',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '29',
        'Fuensanta',
        'Lima Cid',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '51',
        'María Teresa',
        'López Álvarez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '48',
        'Patxi',
        'López Álvarez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '29',
        'Ignacio',
        'López Cano',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '1',
        'Juan Antonio',
        'López de Uralde Garmendia',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '17',
        'Laura',
        'López Domínguez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '19',
        'Ángel',
        'López Maraver',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '30',
        'Antonia',
        'López Moya',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Gema',
        'López Somoza',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '13',
        'Cristina',
        'López Zamora',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '3',
        'Juan Ignacio',
        'López-Bas Valero',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '14',
        'Andrés',
        'Lorite Lorite',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '41',
        'José',
        'Losada Fernández',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Roser',
        'Maestro Moliner',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Percival',
        'Manglano Albacar',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '29',
        'Rubén Silvano',
        'Manso Olivar',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Pilar',
        'Marcos Domínguez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '10',
        'María Dolores',
        'Marcos Moyano',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '34',
        'Milagros',
        'Marcos Ortega',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '17',
        'Joan',
        'Margall Sastre',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '50',
        'Pau',
        'Marí Klose',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '35',
        'Guillermo',
        'Mariscal Anaya',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '45',
        'Manuel',
        'Mariscal Zabala',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '36',
        'María Ángeles',
        'Marra Domínguez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '15',
        'María Valentina',
        'Martínez Ferro',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '11',
        'María Carmen',
        'Martínez Granados',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '33',
        'Isidro Manuel',
        'Martínez Oblanca',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '34',
        'María Luz',
        'Martínez Seijo',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '4',
        'Juan José',
        'Matarí Sáez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '9',
        'Jaime Miguel',
        'Mateu Istúriz',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '48',
        'Oskar',
        'Matute García de Jalón',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Rafael',
        'Mayoral Perales',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '39',
        'José María',
        'Mazón Ramos',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Rosa María',
        'Medel Pérez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '36',
        'Guillermo Antonio',
        'Meijón Couselo',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Joan',
        'Mena Arca',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '30',
        'Lourdes',
        'Méndez Monasterio',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '26',
        'Javier',
        'Merino Martínez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '3',
        'Manuel',
        'Mestre Barea',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '25',
        'Montse',
        'Mínguez García',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '17',
        'Sergi',
        'Miquel i Valentí',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '18',
        'Onofre',
        'Miralles Martín',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '9',
        'María Sandra',
        'Moneo Díez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '41',
        'María Jesús',
        'Montero Cuadrado',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Irene María',
        'Montero Gil',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '3',
        'Macarena',
        'Montesinos de Miguel',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '15',
        'Tristana María',
        'Moraleja Gómez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '37',
        'María Jesús',
        'Moro Almaraz',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '39',
        'Diego',
        'Movellán Lombilla',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '7',
        'Lucía',
        'Muñoz Dalda',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'María',
        'Muñoz Vidal',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '29',
        'María Dolores',
        'Narváez Bandera',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '22',
        'Begoña',
        'Nasarre Oliva',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Julio',
        'Navalpotro Gómez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '2',
        'Carmen',
        'Navarro Lacoba',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '50',
        'Pedro',
        'Navarro López',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '10',
        'María Magdalena',
        'Nevado del Campo',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Míriam',
        'Nogueras i Camero',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '27',
        'Jaime Eduardo de',
        'Olano Vela',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '38',
        'Ana María',
        'Oramas González-Moro',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '18',
        'María Inmaculada',
        'Oria López',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '11',
        'José Ramón',
        'Ortega Domínguez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Francisco Javier',
        'Ortega Smith-Molina',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '11',
        'José',
        'Ortiz Galván',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '45',
        'Esther',
        'Padilla Ruiz',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Josep',
        'Pagès i Massó',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '34',
        'Miguel Ángel',
        'Paniagua Núñez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Ana María',
        'Pastor Julián',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '26',
        'Raquel',
        'Pedraja Sáinz',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '30',
        'Juan Luis',
        'Pedreño Molina',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '9',
        'Esther',
        'Peña Camarero',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'María Mercè',
        'Perea i Conillas',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '32',
        'Adolfo',
        'Pérez Abellás',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '35',
        'María Auxiliadora',
        'Pérez Díaz',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'María Mercedes',
        'Pérez Merino',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '29',
        'Isabel Gema',
        'Pérez Recuerda',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '6',
        'Víctor Valentín',
        'Píriz Maya',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Gerardo',
        'Pisarello Prados',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '35',
        'María del Carmen',
        'Pita Cárdenes',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '7',
        'Pere Joan',
        'Pons Sampietro',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '40',
        'Jesús',
        'Postigo Quintana',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '31',
        'Isabel',
        'Pozueta Fernández',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '27',
        'Ana',
        'Prieto Nieto',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '7',
        'Margarita',
        'Prohens Rigo',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '43',
        'Norma',
        'Pujol i Farré',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Miguel Ángel',
        'Quintanilla Navarro',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Arnau',
        'Ramírez Carner',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '14',
        'José',
        'Ramírez del Río',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '18',
        'Elvira',
        'Ramón Utrabo',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '10',
        'César Joaquín',
        'Ramos Esteban',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '21',
        'José Luis',
        'Ramos Rodríguez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '38',
        'María Tamara',
        'Raya Rodríguez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '14',
        'María de la O',
        'Redondo Calvillo',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '15',
        'Néstor',
        'Rego Candamil',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '12',
        'Germán',
        'Renau Martínez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '49',
        'Pedro Jesús',
        'Requejo Novoa',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '23',
        'Juan Diego',
        'Requena Ruiz',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '45',
        'Carmen',
        'Riolobos Regadera',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '30',
        'Joaquín',
        'Robles López',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '35',
        'Andrés Alberto',
        'Rodríguez Almeida',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '21',
        'María del Pilar',
        'Rodríguez Gómez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '41',
        'Alfonso',
        'Rodríguez Gómez de Celis',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'María Elvira',
        'Rodríguez Herrer',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '18',
        'José Antonio',
        'Rodríguez Salas',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '18',
        'Carlos',
        'Rojas García',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '11',
        'José Ignacio',
        'Romaní Cantera',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '21',
        'Carmelo',
        'Romero Hernández',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '13',
        'Rosa María',
        'Romero Sánchez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '41',
        'María de los Reyes',
        'Romero Vilches',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '12',
        'Susana',
        'Ros Martínez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '11',
        'Agustín',
        'Rosety Fernández de Castro',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Marta',
        'Rosique i Saltor',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '29',
        'Patricia',
        'Rueda Perelló',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Gabriel',
        'Rufián Romero',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '1',
        'Iñaki',
        'Ruiz de Pinedo Undiano',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '43',
        'Joan',
        'Ruiz i Carbonell',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '3',
        'Eduardo Luis',
        'Ruiz Navarro',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'María de la Cabeza',
        'Ruiz Solás',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '12',
        'Marisa',
        'Saavedra Muñoz',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Inés',
        'Sabanés Nadal',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '47',
        'Pablo',
        'Sáez Alonso-Muñumer',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '48',
        'Idoia',
        'Sagastizabal Unzetabarrenetxea',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '16',
        'Luis Carlos',
        'Sahuquillo García',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '7',
        'Antonio',
        'Salvá Verd',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '43',
        'Jordi',
        'Salvador i Duch',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '6',
        'Víctor Manuel',
        'Sánchez del Real',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '6',
        'Mariano',
        'Sánchez Escobar',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '3',
        'José María',
        'Sánchez García',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '30',
        'Marisol',
        'Sánchez Jódar',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '3',
        'César',
        'Sánchez Pérez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Pedro',
        'Sánchez Pérez-Castejón',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '30',
        'Javier',
        'Sánchez Serna',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '22',
        'Alfredo',
        'Sancho Guardia',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '44',
        'Herminio Rufino',
        'Sancho Íñiguez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Luis',
        'Santamaría Ruiz',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Enrique Fernando',
        'Santiago Romero',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Vicent Manuel',
        'Sarrià Morell',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '31',
        'Sergio',
        'Sayas López',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Juan Carlos',
        'Segura Just',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '1',
        'Daniel',
        'Senderos Oraá',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '37',
        'David',
        'Serrada Pariente',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '23',
        'Juan Francisco',
        'Serrano Martínez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '3',
        'Yolanda',
        'Seva Ruiz',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '23',
        'Felipe Jesús',
        'Sicilia Alférez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Rafael',
        'Simancas Simancas',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '3',
        'Alejandro',
        'Soler Mur',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '30',
        'Juan Luis',
        'Soto Burillo',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Juan Luis',
        'Steegmann Olmedillas',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '50',
        'Eloy',
        'Suárez Lamata',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '50',
        'Susana',
        'Sumelzo Jordán',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '15',
        'Diego',
        'Taibo Monelos',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '41',
        'Ricardo',
        'Tarno Blanco',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Carolina',
        'Telechea i Lozano',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '45',
        'Vicente',
        'Tirado Ochoa',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '32',
        'Uxía',
        'Tizón Vázquez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Carla',
        'Toscano de Balbín',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '5',
        'Georgina',
        'Trías Gil',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Edurne',
        'Uriarte Bengoechea',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '48',
        'Roberto',
        'Uriarte Torrealday',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '46',
        'Julio',
        'Utrilla Cano',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Pilar',
        'Vallugera Balañà',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '32',
        'Ana Belén',
        'Vázquez Blanco',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '38',
        'Rubén Darío',
        'Vega Arias',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Mireia',
        'Vehí Cantenys',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '14',
        'Martina',
        'Velarde Gómez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '49',
        'Elvira',
        'Velasco Morillo',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '28',
        'Daniel',
        'Vicente Viondi',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'Aina',
        'Vidal Sáez',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '2',
        'María Luisa',
        'Vilches Ruiz',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '50',
        'Noemí',
        'Villagrasa Quero',
        '0',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '9',
        'Agustín',
        'Zamarrón Moreno',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '11',
        'Carlos José',
        'Zambrano García-Raez',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '19',
        'Aurelio',
        'Zapata Simón',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '8',
        'José',
        'Zaragoza Alonso',
        '1',
        '14'
    );
INSERT INTO
    deputy
(
    circumscription_id,
    name,
    surname,
    is_male,
    deputy_legislature_code
)
VALUES
    (
        '38',
        'Ana María',
        'Zurita Expósito',
        '0',
        '14'
    );