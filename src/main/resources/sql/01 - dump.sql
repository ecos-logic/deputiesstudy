-- MariaDB dump 10.19  Distrib 10.6.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: deputies_chamber
-- ------------------------------------------------------
-- Server version	10.6.12-MariaDB-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `circumscription`
--

DROP TABLE IF EXISTS `circumscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `circumscription` (
  `circumscription_id` smallint(6) NOT NULL,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`circumscription_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `circumscription`
--

LOCK TABLES `circumscription` WRITE;
/*!40000 ALTER TABLE `circumscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `circumscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deputy`
--

DROP TABLE IF EXISTS `deputy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deputy` (
  `deputy_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `circumscription_id` smallint(6) NOT NULL,
  `active_political_party_rel_deputy_id` int(11) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `is_male` tinyint(1) NOT NULL,
  `deputy_legislature_code` smallint(6) NOT NULL,
  PRIMARY KEY (`deputy_id`),
  KEY `deputy_circumscription_FK` (`circumscription_id`),
  KEY `deputy_political_party_rel_deputy_FK` (`active_political_party_rel_deputy_id`),
  CONSTRAINT `deputy_circumscription_FK` FOREIGN KEY (`circumscription_id`) REFERENCES `circumscription` (`circumscription_id`),
  CONSTRAINT `deputy_political_party_rel_deputy_FK` FOREIGN KEY (`active_political_party_rel_deputy_id`) REFERENCES `political_party_rel_deputy` (`political_party_rel_deputy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deputy`
--

LOCK TABLES `deputy` WRITE;
/*!40000 ALTER TABLE `deputy` DISABLE KEYS */;
/*!40000 ALTER TABLE `deputy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `initiative`
--

DROP TABLE IF EXISTS `initiative`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `initiative` (
  `initiative_id` int(11) NOT NULL AUTO_INCREMENT,
  `legislature_id` smallint(6) NOT NULL,
  `identity` varchar(100) NOT NULL,
  `title` varchar(10000) NOT NULL,
  `document` varchar(1000) NOT NULL,
  `result` varchar(100) NOT NULL,
  `qualified_date` date NOT NULL,
  `presented_date` date NOT NULL,
  PRIMARY KEY (`initiative_id`),
  KEY `initiative_legislature_FK` (`legislature_id`),
  CONSTRAINT `initiative_legislature_FK` FOREIGN KEY (`legislature_id`) REFERENCES `legislature` (`legislature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `initiative`
--

LOCK TABLES `initiative` WRITE;
/*!40000 ALTER TABLE `initiative` DISABLE KEYS */;
/*!40000 ALTER TABLE `initiative` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `initiative_rel_author`
--

DROP TABLE IF EXISTS `initiative_rel_author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `initiative_rel_author` (
  `initiative_rel_author_id` int(11) NOT NULL AUTO_INCREMENT,
  `initiative_id` int(11) NOT NULL,
  `deputy_id` smallint(6) DEFAULT NULL,
  `political_party_id` smallint(6) DEFAULT NULL,
  `parliamentary_group_id` smallint(6) DEFAULT NULL,
  `third_party_author_id` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`initiative_rel_author_id`),
  KEY `initiative_rel_author_initiative_FK` (`initiative_id`),
  KEY `initiative_rel_author_deputy_FK` (`deputy_id`),
  KEY `initiative_rel_author_political_party_FK` (`political_party_id`),
  KEY `initiative_rel_author_third_party_author_FK` (`third_party_author_id`),
  KEY `initiative_rel_author_parliamentary_group_FK` (`parliamentary_group_id`),
  CONSTRAINT `initiative_rel_author_deputy_FK` FOREIGN KEY (`deputy_id`) REFERENCES `deputy` (`deputy_id`),
  CONSTRAINT `initiative_rel_author_initiative_FK` FOREIGN KEY (`initiative_id`) REFERENCES `initiative` (`initiative_id`),
  CONSTRAINT `initiative_rel_author_parliamentary_group_FK` FOREIGN KEY (`parliamentary_group_id`) REFERENCES `parliamentary_group` (`parliamentary_group_id`),
  CONSTRAINT `initiative_rel_author_political_party_FK` FOREIGN KEY (`political_party_id`) REFERENCES `political_party` (`political_party_id`),
  CONSTRAINT `initiative_rel_author_third_party_author_FK` FOREIGN KEY (`third_party_author_id`) REFERENCES `third_party_author` (`third_party_author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `initiative_rel_author`
--

LOCK TABLES `initiative_rel_author` WRITE;
/*!40000 ALTER TABLE `initiative_rel_author` DISABLE KEYS */;
/*!40000 ALTER TABLE `initiative_rel_author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intervention`
--

DROP TABLE IF EXISTS `intervention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intervention` (
  `intervention_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `legislature_id` smallint(6) NOT NULL,
  `speaker_charge` varchar(100) NOT NULL,
  `speaker` varchar(250) NOT NULL COMMENT 'no oficial SEE INTERVENTION_REL_SPEAKER',
  `document_link` varchar(1000) NOT NULL,
  `document_code` varchar(1000) NOT NULL,
  `document_format` varchar(1000) NOT NULL,
  `source` varchar(500) NOT NULL,
  `identity` varchar(1000) NOT NULL,
  `title` varchar(10000) NOT NULL,
  PRIMARY KEY (`intervention_id`),
  KEY `intervention_legislature_FK` (`legislature_id`),
  CONSTRAINT `intervention_legislature_FK` FOREIGN KEY (`legislature_id`) REFERENCES `legislature` (`legislature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intervention`
--

LOCK TABLES `intervention` WRITE;
/*!40000 ALTER TABLE `intervention` DISABLE KEYS */;
/*!40000 ALTER TABLE `intervention` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intervention_rel_author`
--

DROP TABLE IF EXISTS `intervention_rel_author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intervention_rel_author` (
  `intervention_rel_author_id` int(11) NOT NULL AUTO_INCREMENT,
  `intervention_id` int(11) NOT NULL,
  `deputy_id` smallint(6) DEFAULT NULL,
  `political_party_id` smallint(6) DEFAULT NULL,
  `parliamentary_group_id` smallint(6) DEFAULT NULL,
  `third_party_author_id` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`intervention_rel_author_id`),
  KEY `intervention_rel_author_intervention_FK` (`intervention_id`),
  KEY `intervention_rel_author_deputy_FK` (`deputy_id`),
  KEY `intervention_rel_author_political_party_FK` (`political_party_id`),
  KEY `intervention_rel_author_third_party_FK` (`third_party_author_id`),
  KEY `intervention_rel_author_parliamentary_group_FK` (`parliamentary_group_id`),
  CONSTRAINT `intervention_rel_author_deputy_FK` FOREIGN KEY (`deputy_id`) REFERENCES `deputy` (`deputy_id`),
  CONSTRAINT `intervention_rel_author_intervention_FK` FOREIGN KEY (`intervention_id`) REFERENCES `intervention` (`intervention_id`),
  CONSTRAINT `intervention_rel_author_parliamentary_group_FK` FOREIGN KEY (`parliamentary_group_id`) REFERENCES `parliamentary_group` (`parliamentary_group_id`),
  CONSTRAINT `intervention_rel_author_political_party_FK` FOREIGN KEY (`political_party_id`) REFERENCES `political_party` (`political_party_id`),
  CONSTRAINT `intervention_rel_author_third_party_FK` FOREIGN KEY (`third_party_author_id`) REFERENCES `third_party_author` (`third_party_author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intervention_rel_author`
--

LOCK TABLES `intervention_rel_author` WRITE;
/*!40000 ALTER TABLE `intervention_rel_author` DISABLE KEYS */;
/*!40000 ALTER TABLE `intervention_rel_author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intervention_video`
--

DROP TABLE IF EXISTS `intervention_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intervention_video` (
  `intervention_video_id` int(11) NOT NULL COMMENT 'SAME ID AS INTERVENTION',
  `time_code` varchar(100) NOT NULL,
  `url` varchar(2500) NOT NULL,
  `download_link` varchar(2500) NOT NULL,
  `broadcast_link` varchar(2500) NOT NULL,
  `sequence` varchar(500) NOT NULL,
  `start_code` varchar(500) NOT NULL,
  `identity` varchar(500) NOT NULL,
  `source` varchar(500) NOT NULL,
  PRIMARY KEY (`intervention_video_id`),
  CONSTRAINT `intervention_video_is_intervention_FK` FOREIGN KEY (`intervention_video_id`) REFERENCES `intervention` (`intervention_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intervention_video`
--

LOCK TABLES `intervention_video` WRITE;
/*!40000 ALTER TABLE `intervention_video` DISABLE KEYS */;
/*!40000 ALTER TABLE `intervention_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `legislature`
--

DROP TABLE IF EXISTS `legislature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `legislature` (
  `legislature_id` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`legislature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `legislature`
--

LOCK TABLES `legislature` WRITE;
/*!40000 ALTER TABLE `legislature` DISABLE KEYS */;
/*!40000 ALTER TABLE `legislature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parliamentary_group`
--

DROP TABLE IF EXISTS `parliamentary_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parliamentary_group` (
  `parliamentary_group_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(500) NOT NULL,
  PRIMARY KEY (`parliamentary_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parliamentary_group`
--

LOCK TABLES `parliamentary_group` WRITE;
/*!40000 ALTER TABLE `parliamentary_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `parliamentary_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parliamentary_group_by_legislature`
--

DROP TABLE IF EXISTS `parliamentary_group_by_legislature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parliamentary_group_by_legislature` (
  `parliamentary_group_by_legislature_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `parliamentary_group_id` smallint(6) NOT NULL,
  `political_party_id` smallint(6) NOT NULL,
  `legislature_id` smallint(6) NOT NULL,
  PRIMARY KEY (`parliamentary_group_by_legislature_id`),
  UNIQUE KEY `parliamentary_group_by_legislature_UK` (`parliamentary_group_id`,`political_party_id`,`legislature_id`),
  KEY `parliamentary_group_by_legislature_political_party_FK` (`political_party_id`),
  KEY `parliamentary_group_by_legislature_legislature_FK` (`legislature_id`),
  CONSTRAINT `parliamentary_group_by_legislature_legislature_FK` FOREIGN KEY (`legislature_id`) REFERENCES `legislature` (`legislature_id`),
  CONSTRAINT `parliamentary_group_by_legislature_parliamentary_group_FK` FOREIGN KEY (`parliamentary_group_id`) REFERENCES `parliamentary_group` (`parliamentary_group_id`),
  CONSTRAINT `parliamentary_group_by_legislature_political_party_FK` FOREIGN KEY (`political_party_id`) REFERENCES `political_party` (`political_party_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parliamentary_group_by_legislature`
--

LOCK TABLES `parliamentary_group_by_legislature` WRITE;
/*!40000 ALTER TABLE `parliamentary_group_by_legislature` DISABLE KEYS */;
/*!40000 ALTER TABLE `parliamentary_group_by_legislature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `political_party`
--

DROP TABLE IF EXISTS `political_party`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `political_party` (
  `political_party_id` smallint(6) NOT NULL,
  `short_name` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`political_party_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `political_party`
--

LOCK TABLES `political_party` WRITE;
/*!40000 ALTER TABLE `political_party` DISABLE KEYS */;
/*!40000 ALTER TABLE `political_party` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `political_party_rel_deputy`
--

DROP TABLE IF EXISTS `political_party_rel_deputy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `political_party_rel_deputy` (
  `political_party_rel_deputy_id` int(11) NOT NULL AUTO_INCREMENT,
  `deputy_id` smallint(6) NOT NULL,
  `political_party_id` smallint(6) NOT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`political_party_rel_deputy_id`),
  KEY `political_party_rel_deputy_deputy_FK` (`deputy_id`),
  KEY `political_party_rel_deputy_political_party_FK` (`political_party_id`),
  CONSTRAINT `political_party_rel_deputy_deputy_FK` FOREIGN KEY (`deputy_id`) REFERENCES `deputy` (`deputy_id`),
  CONSTRAINT `political_party_rel_deputy_political_party_FK` FOREIGN KEY (`political_party_id`) REFERENCES `political_party` (`political_party_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `political_party_rel_deputy`
--

LOCK TABLES `political_party_rel_deputy` WRITE;
/*!40000 ALTER TABLE `political_party_rel_deputy` DISABLE KEYS */;
/*!40000 ALTER TABLE `political_party_rel_deputy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `third_party_author`
--

DROP TABLE IF EXISTS `third_party_author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `third_party_author` (
  `third_party_author_id` smallint(6) NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`third_party_author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `third_party_author`
--

LOCK TABLES `third_party_author` WRITE;
/*!40000 ALTER TABLE `third_party_author` DISABLE KEYS */;
/*!40000 ALTER TABLE `third_party_author` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-25 14:26:29
