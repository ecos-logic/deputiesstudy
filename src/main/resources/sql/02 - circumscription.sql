INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        1,
        'Araba/Álava'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        2,
        'Albacete'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        3,
        'Alicante/Alacant'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        4,
        'Almería'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        5,
        'Ávila'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        6,
        'Badajoz'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        7,
        'Balears (Illes)'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        8,
        'Barcelona'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        9,
        'Burgos'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        10,
        'Cáceres'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        11,
        'Cádiz'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        12,
        'Castellón/Castelló'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        13,
        'Ciudad Real'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        14,
        'Córdoba'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        15,
        'Coruña (A)'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        16,
        'Cuenca'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        17,
        'Girona'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        18,
        'Granada'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        19,
        'Guadalajara'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        20,
        'Gipuzkoa'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        21,
        'Huelva'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        22,
        'Huesca'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        23,
        'Jaén'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        24,
        'León'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        25,
        'Lleida'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        26,
        'Rioja (La)'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        27,
        'Lugo'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        28,
        'Madrid'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        29,
        'Málaga'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        30,
        'Murcia'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        31,
        'Navarra'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        32,
        'Ourense'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        33,
        'Asturias'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        34,
        'Palencia'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        35,
        'Palmas (Las)'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        36,
        'Pontevedra'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        37,
        'Salamanca'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        38,
        'S/C Tenerife'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        39,
        'Cantabria'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        40,
        'Segovia'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        41,
        'Sevilla'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        42,
        'Soria'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        43,
        'Tarragona'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        44,
        'Teruel'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        45,
        'Toledo'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        46,
        'Valencia/València'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        47,
        'Valladolid'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        48,
        'Bizkaia'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        49,
        'Zamora'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        50,
        'Zaragoza'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        51,
        'Ceuta'
    );
INSERT INTO
    circumscription
(
    circumscription_id,
    name
)
VALUES
    (
        52,
        'Melilla'
    );
