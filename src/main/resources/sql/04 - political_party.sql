INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        1,
        'PP',
        'PP'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        2,
        'PsdeG-PSOE',
        'PsdeG-PSOE'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        3,
        'NC-CCa-PNC',
        'NC-CCa-PNC'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        4,
        'PRC',
        'PRC'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        5,
        'EC-UP',
        'EC-UP'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        6,
        'PSE-EE-PSOE',
        'PSE-EE-PSOE'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        7,
        'JxCat-JUNTS (PDeCAT)',
        'JxCat-JUNTS (PDeCAT)'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        8,
        '¡Teruel Existe!',
        '¡Teruel Existe!'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        9,
        'ERC-S',
        'ERC-S'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        10,
        'JxCat-JUNTS (Junts)',
        'JxCat-JUNTS (Junts)'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        11,
        'MÁS PAÍS-EQUO',
        'MÁS PAÍS-EQUO'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        12,
        'Cs',
        'Cs'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        13,
        'PSC-PSOE',
        'PSC-PSOE'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        14,
        'CCa-PNC-NC',
        'CCa-PNC-NC'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        15,
        'EAJ-PNV',
        'EAJ-PNV'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        16,
        'EH Bildu',
        'EH Bildu'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        17,
        'NA+',
        'NA+'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        18,
        'UP',
        'UP'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        19,
        'PP-FORO',
        'PP-FORO'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        20,
        'ECP-GUAYEM EL CANVI',
        'ECP-GUAYEM EL CANVI'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        21,
        'CUP-PR',
        'CUP-PR'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        22,
        'MÉS COMPROMÍS',
        'MÉS COMPROMÍS'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        23,
        'BNG',
        'BNG'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        24,
        'PSOE',
        'PSOE'
    );
INSERT INTO
    political_party
(
    political_party_id,
    short_name,
    name
)
VALUES
    (
        25,
        'Vox',
        'Vox'
    );
