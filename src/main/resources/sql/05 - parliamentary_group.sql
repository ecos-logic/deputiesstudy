INSERT INTO
    parliamentary_group
(
    parliamentary_group_id,
    group_name
)
VALUES
    (
        1,
        'Grupo Parlamentario Plural'
    );
INSERT INTO
    parliamentary_group
(
    parliamentary_group_id,
    group_name
)
VALUES
    (
        2,
        'Grupo Parlamentario Popular en el Congreso'
    );
INSERT INTO
    parliamentary_group
(
    parliamentary_group_id,
    group_name
)
VALUES
    (
        3,
        'Grupo Parlamentario Republicano'
    );
INSERT INTO
    parliamentary_group
(
    parliamentary_group_id,
    group_name
)
VALUES
    (
        4,
        'Grupo Parlamentario Socialista'
    );
INSERT INTO
    parliamentary_group
(
    parliamentary_group_id,
    group_name
)
VALUES
    (
        5,
        'Grupo Parlamentario VOX'
    );
INSERT INTO
    parliamentary_group
(
    parliamentary_group_id,
    group_name
)
VALUES
    (
        6,
        'Grupo Parlamentario Ciudadanos'
    );
INSERT INTO
    parliamentary_group
(
    parliamentary_group_id,
    group_name
)
VALUES
    (
        7,
        'Grupo Parlamentario Confederal de Unidas Podemos-En Comú Podem-Galicia en Común'
    );
INSERT INTO
    parliamentary_group
(
    parliamentary_group_id,
    group_name
)
VALUES
    (
        8,
        'Grupo Parlamentario Euskal Herria Bildu'
    );
INSERT INTO
    parliamentary_group
(
    parliamentary_group_id,
    group_name
)
VALUES
    (
        9,
        'Grupo Parlamentario Vasco (EAJ-PNV)'
    );
INSERT INTO
    parliamentary_group
(
    parliamentary_group_id,
    group_name
)
VALUES
    (
        10,
        'Grupo Parlamentario Mixto'
    );
