#!/bin/bash

while IFS= read -r line; do
  # echo "tester: $line"
  echo Initiatives: $line: $(find ../initiatives/20230402_2000/ -name "*.formatted.json" -exec grep "$line" '{}' \; | wc --lines)
  echo Interventions: $line: $(find ../interventions/20230402_1700/ -name "*.formatted.json" -exec grep "$line" '{}' \; | wc --lines)

done < "deputiesClear"

