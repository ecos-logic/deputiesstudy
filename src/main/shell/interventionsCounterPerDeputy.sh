#!/bin/bash

while IFS= read -r line; do
  # echo "tester: $line"
  echo Interventions: $line: $(find ../interventions/20230402_1700/ -name "*.formatted.json" -exec grep "\"orador\": \"$line" '{}' \; | wc --lines)

done < "deputiesClear"

