package org.ecos.logic.deputies.study.initiatives;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.ecos.logic.deputies.study.CommonResourceFunctionality;
import org.ecos.logic.deputies.study.initiative.Initiative;
import org.ecos.logic.deputies.study.policitalparty.Deputy;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.ecos.logic.deputies.study.CommonResourceFunctionality.writeTo;
import static org.ecos.logic.deputies.study.data.entity.CommonConstant.LEGISLATURE_XIV_ID;
import static org.ecos.logic.deputies.study.object_mother.DeputyObjectMother.getDeputyCollection;
import static org.ecos.logic.deputies.study.object_mother.ParliamentaryGroupObjectMother.getParliamentaryGroupNameAndIdCollection;
import static org.ecos.logic.deputies.study.object_mother.PoliticalPartyObjectMother.getNameAndIdCollection;
import static org.ecos.logic.deputies.study.object_mother.ThirdPartyAuthorObjectMother.getThirdPartyAuthorNameAndIdCollection;

class InitiativesFromJsonTest {
    public static final int INITIATIVES_COUNTER = 142389;

    @Test
    void fromJSONInitiativesToInitiativesList() throws IOException, ParseException {
        List<Initiative> initiatives = getFromJsonAndAssert();
        Optional<Initiative> initiative = initiatives.stream().filter(i -> i.getIdentity().equals("084/000063")).findFirst();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        initiative.ifPresent(value -> {
            try {
                assertThat(formatter.format(parseDateFrom(value.getQualifiedDate()))).isEqualTo("2023-02-28");
                assertThat(value.getQualifiedDate()).isEqualTo("28/02/2023");


                String builder = "INSERT INTO \n \t" +
                        "initiative\n" +
                        "(\n\t" +
                        "legislature_id,\n\t" +
                        "identity,\n\t" +
                        "title,\n\t" +
                        "document,\n\t" +
                        "result,\n\t" +
                        "qualified_date,\n\t" +
                        "presented_date\n" +
                        ")\n" +
                        "VALUES\n" +
                        "(\n\t" +
                        LEGISLATURE_XIV_ID +
                        ",\n\t'" +
                        value.getIdentity().replace("'", "\\'") +
                        "',\n\t'" +
                        value.getTitle().replace("'", "\\'") +
                        "',\n\t'" +
                        value.getDocument().replace("'", "\\'") +
                        "',\n\t'" +
                        value.getResult().replace("'", "\\'") +
                        "',\n\t'" +
                        formatter.format(parseDateFrom(value.getQualifiedDate())) +
                        "',\n\t'" +
                        formatter.format(parseDateFrom(value.getPresentedDate())) +
                        "'\n);";

                assertThat(builder).
                    contains("2023-02-28").
                    contains("084/000063");

            } catch (ParseException e) {
                throw new RuntimeException(e);
            }

        });
    }

    @Test
    void fromJSONInitiativesToInsertInto() throws IOException, ParseException {
        List<Initiative> initiatives = getFromJsonAndAssert();

        List<StringBuilder> insertBuilder = new ArrayList<>();

        for (Initiative initiative : initiatives) {
            StringBuilder builder = new StringBuilder();
            builder.append("INSERT INTO \n \t" +
                    "initiative\n" +
                    "(\n\t" +
                    "legislature_id,\n\t" +
                    "identity,\n\t" +
                    "title,\n\t" +
                    "document,\n\t" +
                    "result,\n\t" +
                    "qualified_date,\n\t" +
                    "presented_date\n" +
                    ")\n" +
                    "VALUES\n" +
                    "(\n\t");
            builder.append(LEGISLATURE_XIV_ID);
            builder.append(",\n\t'");
            builder.append(initiative.getIdentity().replace("'", "\\'"));
            builder.append("',\n\t'");
            builder.append(initiative.getTitle().replace("'", "\\'"));
            builder.append("',\n\t'");
            builder.append(initiative.getDocument().replace("'", "\\'"));
            builder.append("',\n\t'");
            builder.append(initiative.getResult().replace("'", "\\'"));
            builder.append("',\n\t'");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            builder.append(formatter.format(parseDateFrom(initiative.getQualifiedDate())));
            builder.append("',\n\t'");
            builder.append(formatter.format(parseDateFrom(initiative.getPresentedDate())));
            builder.append("'\n);\n\n");
            insertBuilder.add(builder);
        }
        StringBuilder allText = new StringBuilder();
        for (StringBuilder builder : insertBuilder) {
            allText.append(builder.toString());
        }
        writeTo("/sql/09 - initiative.sql", allText.toString());

    }

    @Test
    void generatingInitiativeAuthors() throws IOException {
        String result = CommonResourceFunctionality.readFromRecourcesFileToString("/initiatives/allInitiatives.json");
        Gson gson = new Gson();
        Type listOfMyClassObject = new TypeToken<ArrayList<Initiative>>() {
        }.getType();
        List<Initiative> initiatives = gson.fromJson(result, listOfMyClassObject);

        Map<String, Integer> politicalPartyNameAndIdCollection = getNameAndIdCollection();
        Map<String, Integer> parliamentaryGroupNameAndId = getParliamentaryGroupNameAndIdCollection();
        Map<String, Integer> thirdPartyAuthorNameAndId = getThirdPartyAuthorNameAndIdCollection();
        assertThat(thirdPartyAuthorNameAndId.size()).isEqualTo(228);
        Map<org.ecos.logic.deputies.study.policitalparty.Deputy, Integer> deputyCollection = getDeputyCollection();

        StringBuilder stringBuilder = new StringBuilder();
        for (Initiative initiative : initiatives) {
            List<String> initiativeAuthors = initiative.getDetailedAuthors();
            String identity = initiative.getIdentity();
            for (String author : initiativeAuthors) {
                Optional<Map.Entry<Deputy, Integer>> optionalDeputyAuthor = deputyCollection.entrySet().stream().filter(deputyIntegerEntry -> deputyIntegerEntry.getKey().getSurnameAndName().equals(author)).findFirst();
                if (optionalDeputyAuthor.isPresent()) {
                    Map.Entry<Deputy, Integer> deputyAuthor = optionalDeputyAuthor.get();
                    stringBuilder.append(format("INSERT INTO\n" +
                            "\tinitiative_rel_author\n" +
                            "(\n" +
                            "\tinitiative_id,\n" +
                            "\tdeputy_id\n" +
                            ")\n" +
                            "SELECT\n" +
                            "\tinitiative_id,\n" +
                            "\t%s as deputy_id\n" +
                            "FROM\n" +
                            "\tinitiative\n" +
                            "WHERE\n" +
                            "\tidentity = '%s'\n", deputyAuthor.getValue(), identity));
                } else {
                    Optional<Map.Entry<String, Integer>> optionalPoliticalPartyAuthor = politicalPartyNameAndIdCollection.entrySet().stream().filter(pl -> pl.getKey().equals(author)).findFirst();
                    if (optionalPoliticalPartyAuthor.isPresent()) {
                        Map.Entry<String, Integer> politicalPartyAuthor = optionalPoliticalPartyAuthor.get();
                        stringBuilder.append(format("INSERT INTO\n" +
                                "\tinitiative_rel_author\n" +
                                "(\n" +
                                "\tinitiative_id,\n" +
                                "\tpolitical_party_id\n" +
                                ")\n" +
                                "SELECT\n" +
                                "\tinitiative_id,\n" +
                                "\t%s as political_party_id\n" +
                                "FROM\n" +
                                "\tinitiative\n" +
                                "WHERE\n" +
                                "\tidentity = '%s'\n", politicalPartyAuthor.getValue(), identity));
                    } else {
                        Optional<Map.Entry<String, Integer>> optionalParliamentaryGroupAuthor = parliamentaryGroupNameAndId.entrySet().stream().filter(pl -> pl.getKey().equals(author)).findFirst();
                        if (optionalParliamentaryGroupAuthor.isPresent()) {
                            Map.Entry<String, Integer> parliamentaryGroup = optionalParliamentaryGroupAuthor.get();
                            stringBuilder.append(format("INSERT INTO\n" +
                                    "\tinitiative_rel_author\n" +
                                    "(\n" +
                                    "\tinitiative_id,\n" +
                                    "\tparliamentary_group_id\n" +
                                    ")\n" +
                                    "SELECT\n" +
                                    "\tinitiative_id,\n" +
                                    "\t%s as parliamentary_group_id\n" +
                                    "FROM\n" +
                                    "\tinitiative\n" +
                                    "WHERE\n" +
                                    "\tidentity = '%s'\n", parliamentaryGroup.getValue(), identity));
                        } else {
                            Optional<Map.Entry<String, Integer>> thirdPartyAuthor = thirdPartyAuthorNameAndId.entrySet().stream().filter(pl -> pl.getKey().equals(author)).findFirst();
                            thirdPartyAuthor.ifPresent(stringIntegerEntry -> stringBuilder.append(format("INSERT INTO\n" +
                                    "\tinitiative_rel_author\n" +
                                    "(\n" +
                                    "\tinitiative_id,\n" +
                                    "\tthird_party_author_id\n" +
                                    ")\n" +
                                    "SELECT\n" +
                                    "\tinitiative_id,\n" +
                                    "\t%s as third_party_author_id\n" +
                                    "FROM\n" +
                                    "\tinitiative\n" +
                                    "WHERE\n" +
                                    "\tidentity = '%s'\n", stringIntegerEntry.getValue(), identity)));
                        }
                    }
                }
            }
        }

        CommonResourceFunctionality.writeTo("/sql/11 - initiative_rel_author.sql", stringBuilder.toString());
    }

    @Test
    void getAllAuthors() throws IOException {
        String result = CommonResourceFunctionality.readFromRecourcesFileToString("/initiatives/allInitiatives.json");
        Gson gson = new Gson();
        Type listOfMyClassObject = new TypeToken<ArrayList<Initiative>>() {
        }.getType();
        List<Initiative> initiatives = gson.fromJson(result, listOfMyClassObject);

        Set<String> authors = initiatives.stream().flatMap(i -> i.getDetailedAuthors().stream()).collect(Collectors.toSet());

        StringBuilder authorsAsStringBuilder = new StringBuilder();
        for (String author : authors.stream().sorted().collect(Collectors.toList())) {
            authorsAsStringBuilder.append(author);
            authorsAsStringBuilder.append("\n");
        }
        authorsAsStringBuilder.deleteCharAt(authorsAsStringBuilder.length() - 1);


        writeTo("/initiatives/authors", authorsAsStringBuilder.toString());

        assertThat(authors.size()).isEqualTo(583);
    }

    private static List<Initiative> getFromJsonAndAssert() throws IOException {
        String result = CommonResourceFunctionality.readFromRecourcesFileToString("/initiatives/allInitiatives.json");
        Gson gson = new Gson();
        Type listOfMyClassObject = new TypeToken<ArrayList<Initiative>>() {
        }.getType();
        List<Initiative> initiatives = gson.fromJson(result, listOfMyClassObject);

        assertThat(initiatives.size()).isEqualTo(INITIATIVES_COUNTER);
        return initiatives;
    }

    private static Date parseDateFrom(String dateAsStringDdMMYyyy) throws ParseException {
        Date date;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = format.parse(dateAsStringDdMMYyyy);
        } catch (IllegalArgumentException | ParseException e) {
            date = format.parse("01/01/1977");
        }
        return date;
    }
}
