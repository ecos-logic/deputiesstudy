package org.ecos.logic.deputies.study.initiatives;

import org.ecos.logic.deputies.study.CommonResourceFunctionality;
import org.ecos.logic.deputies.study.initiative.Initiative;
import org.ecos.logic.deputies.study.rest.client.InitiativeInvocation;
import org.json.JSONArray;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

class InitiativeInvocationTest {
    public static final int NUMBER_OF_INITIATIVES_PER_INVOCATION = 25;
    private final InitiativeInvocation invocation = new InitiativeInvocation();

    @Test
    void getAllInitiativesCounter(){
        int initiativeTotal = this.invocation.getInitiativesCounter();
        assertThat(initiativeTotal).isEqualTo(142524);
    }

    @Test
    void getAllInitiatives() throws IOException {
        int initiativeTotal = this.invocation.getInitiativesCounter();

        int counter = initiativeTotal / NUMBER_OF_INITIATIVES_PER_INVOCATION;
        if (initiativeTotal % NUMBER_OF_INITIATIVES_PER_INVOCATION > 0)
            counter++;

        List<Initiative> result = invocation.getAllInitiatives(counter);

        JSONArray jsonArray = new JSONArray(result);
        CommonResourceFunctionality.writeTo("/initiatives/allInitiatives.json", jsonArray.toString(4));
        assertThat(result).hasSize(initiativeTotal);
    }

}
