package org.ecos.logic.deputies.study.object_mother;

import java.util.HashMap;
import java.util.Map;

public class ParliamentaryGroupObjectMother {
    public static Map<String, Integer> getParliamentaryGroupNameAndIdCollection(){
        Map<String, Integer> parliamentaryGroupNameAndId = new HashMap<>();
        parliamentaryGroupNameAndId.put("Grupo Parlamentario Plural",1);
        parliamentaryGroupNameAndId.put("Grupo Parlamentario Popular en el Congreso",2);
        parliamentaryGroupNameAndId.put("Grupo Parlamentario Republicano",3);
        parliamentaryGroupNameAndId.put("Grupo Parlamentario Socialista",4);
        parliamentaryGroupNameAndId.put("Grupo Parlamentario VOX",5);
        parliamentaryGroupNameAndId.put("Grupo Parlamentario Ciudadanos",6);
        parliamentaryGroupNameAndId.put("Grupo Parlamentario Confederal de Unidas Podemos-En Comú Podem-Galicia en Común",7);
        parliamentaryGroupNameAndId.put("Grupo Parlamentario Euskal Herria Bildu",8);
        parliamentaryGroupNameAndId.put("Grupo Parlamentario Vasco (EAJ-PNV)",9);
        parliamentaryGroupNameAndId.put("Grupo Parlamentario Mixto",10);

        return parliamentaryGroupNameAndId;
    }

}
