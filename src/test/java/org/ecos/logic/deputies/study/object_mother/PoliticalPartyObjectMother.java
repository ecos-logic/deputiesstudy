package org.ecos.logic.deputies.study.object_mother;

import java.util.HashMap;
import java.util.Map;

public class PoliticalPartyObjectMother {

    public static Map<String, Integer> getNameAndIdCollection(){
        Map<String, Integer> politicalPartyNameAndId = new HashMap<>();
        politicalPartyNameAndId.put("PP", 1);
        politicalPartyNameAndId.put("PsdeG-PSOE", 2);
        politicalPartyNameAndId.put("NC-CCa-PNC", 3);
        politicalPartyNameAndId.put("PRC", 4);
        politicalPartyNameAndId.put("EC-UP", 5);
        politicalPartyNameAndId.put("PSE-EE-PSOE", 6);
        politicalPartyNameAndId.put("JxCat-JUNTS (PDeCAT)", 7);
        politicalPartyNameAndId.put("¡Teruel Existe!", 8);
        politicalPartyNameAndId.put("ERC-S", 9);
        politicalPartyNameAndId.put("JxCat-JUNTS (Junts)", 10);
        politicalPartyNameAndId.put("MÁS PAÍS-EQUO", 11);
        politicalPartyNameAndId.put("Cs", 12);
        politicalPartyNameAndId.put("PSC-PSOE", 13);
        politicalPartyNameAndId.put("CCa-PNC-NC", 14);
        politicalPartyNameAndId.put("EAJ-PNV", 15);
        politicalPartyNameAndId.put("EH Bildu", 16);
        politicalPartyNameAndId.put("NA+", 17);
        politicalPartyNameAndId.put("UP", 18);
        politicalPartyNameAndId.put("PP-FORO", 19);
        politicalPartyNameAndId.put("ECP-GUAYEM EL CANVI", 20);
        politicalPartyNameAndId.put("CUP-PR", 21);
        politicalPartyNameAndId.put("MÉS COMPROMÍS", 22);
        politicalPartyNameAndId.put("BNG", 23);
        politicalPartyNameAndId.put("PSOE", 24);
        politicalPartyNameAndId.put("Vox", 25);

        return politicalPartyNameAndId;
    }
}
