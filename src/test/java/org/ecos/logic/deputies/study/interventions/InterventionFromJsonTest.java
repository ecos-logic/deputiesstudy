package org.ecos.logic.deputies.study.interventions;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.ecos.logic.deputies.study.CommonResourceFunctionality;
import org.ecos.logic.deputies.study.intervention.Intervention;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Java6Assertions.assertThat;

class InterventionFromJsonTest {

    @Test
    void howManyInterventionWeHave() throws IOException {
        List<Intervention> interventionList;

        String result = CommonResourceFunctionality.readFromRecourcesFileToString("/interventions/allInterventions.json");
        Gson gson = new Gson();
        Type listOfMyClassObject = new TypeToken<ArrayList<Intervention>>() {}.getType();
        interventionList = gson.fromJson(result,listOfMyClassObject);

        assertThat(interventionList).hasSize(80541);

        List<Intervention> interventionWithMoreThanOneAuthor = interventionList.stream().filter(intervention -> intervention.getDetail().getAuthorList().size() > 1).collect(Collectors.toList());
        assertThat(interventionWithMoreThanOneAuthor.size()).isEqualTo(4017);


        List<Intervention> interventionWithMoreThanOneAuthorOfGVOX = interventionWithMoreThanOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("GVOX")).collect(Collectors.toList());
        interventionWithMoreThanOneAuthorOfGVOX.addAll(interventionWithMoreThanOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("Grupo Parlamentario VOX")).collect(Collectors.toList()));
        assertThat(interventionWithMoreThanOneAuthorOfGVOX.size()).isEqualTo(169);


        List<Intervention> interventionWithMoreThanOneAuthorOfGPopularParty = interventionWithMoreThanOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("(GP)")).collect(Collectors.toList());
        interventionWithMoreThanOneAuthorOfGPopularParty.addAll(interventionWithMoreThanOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("Grupo Parlamentario Popular en el Congreso")).collect(Collectors.toList()));
        assertThat(interventionWithMoreThanOneAuthorOfGPopularParty.size()).isEqualTo(728);


        List<Intervention> interventionWithMoreThanOneAuthorOfGS = interventionWithMoreThanOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("(GS)")).collect(Collectors.toList());
        interventionWithMoreThanOneAuthorOfGS.addAll(interventionWithMoreThanOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("Grupo Parlamentario Socialista")).collect(Collectors.toList()));
        assertThat(interventionWithMoreThanOneAuthorOfGS.size()).isEqualTo(2659);


        List<Intervention> interventionWithMoreThanOneAuthorOfGEHBildu = interventionWithMoreThanOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("Bildu")).collect(Collectors.toList());
        assertThat(interventionWithMoreThanOneAuthorOfGEHBildu.size()).isEqualTo(445);


        List<Intervention> interventionWithMoreThanOneAuthorOfCatalonianRepublicanLeftParty = interventionWithMoreThanOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("(GR)")).collect(Collectors.toList());
        assertThat(interventionWithMoreThanOneAuthorOfCatalonianRepublicanLeftParty.size()).isEqualTo(0);


        List<Intervention> interventionWithMoreThanOneAuthorOfMixParty = interventionWithMoreThanOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("(GMx)")).collect(Collectors.toList());
        assertThat(interventionWithMoreThanOneAuthorOfMixParty).hasSize(0);

        List<Intervention> interventionWithOneAuthor = interventionList.stream().filter(intervention -> intervention.getDetail().getAuthorList().size() == 1).collect(Collectors.toList());
        assertThat(interventionWithOneAuthor.size()).isEqualTo(80541-4809);


        List<Intervention> interventionWithOneAuthorOfGVOX = interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("GVOX")).collect(Collectors.toList());
        interventionWithOneAuthorOfGVOX.addAll(interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("Grupo Parlamentario VOX")).collect(Collectors.toList()));
        assertThat(interventionWithOneAuthorOfGVOX.size()).isEqualTo(12371);


        List<Intervention> interventionWithOneAuthorOfSocialistParty = interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("(GS)")).collect(Collectors.toList());
        interventionWithOneAuthorOfSocialistParty.addAll(interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("Grupo Parlamentario Socialista")).collect(Collectors.toList()));
        assertThat(interventionWithOneAuthorOfSocialistParty.size()).isEqualTo(4662);


        List<Intervention> interventionWithOneAuthorOfPeopleParty = interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("(GP)")).collect(Collectors.toList());
        interventionWithOneAuthorOfPeopleParty.addAll(interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("Grupo Parlamentario Popular en el Congreso")).collect(Collectors.toList()));
        assertThat(interventionWithOneAuthorOfPeopleParty.size()).isEqualTo(19386);


        List<Intervention> interventionWithOneAuthorOfGalizianNationalistBlocParty = interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("Rego Candamil, Néstor")).collect(Collectors.toList());
        interventionWithOneAuthorOfGalizianNationalistBlocParty.addAll(interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasSpeakerThatContains("Rego Candamil, Néstor")).collect(Collectors.toList()));
        assertThat(interventionWithOneAuthorOfGalizianNationalistBlocParty.size()).isEqualTo(782);

        List<Intervention> interventionWithOneAuthorOfCalanoianRepublicanLeftParty = interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("(GR)")).collect(Collectors.toList());
        interventionWithOneAuthorOfCalanoianRepublicanLeftParty.addAll(interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasSpeakerThatContains("(GR)")).collect(Collectors.toList()));
        assertThat(interventionWithOneAuthorOfCalanoianRepublicanLeftParty.size()).isEqualTo(4044);


        List<Intervention> interventionWithOneAuthorOfWeCanParty = interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("Grupo Parlamentario Confederal de Unidas Podemos-En Comú Podem-Galicia en Común")).collect(Collectors.toList());
        interventionWithOneAuthorOfWeCanParty.addAll(interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("(GCUP-EC-GC)")).collect(Collectors.toList()));
        interventionWithOneAuthorOfWeCanParty.addAll(interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasSpeakerThatContains("(GCUP-EC-GC)")).collect(Collectors.toList()));
        assertThat(interventionWithOneAuthorOfWeCanParty.size()).isEqualTo(9677);

        List<Intervention> interventionWithOneAuthorCalledTamames = interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasAuthorThatContains("Tamames")).collect(Collectors.toList());
        interventionWithOneAuthorCalledTamames.addAll(interventionWithOneAuthor.stream().filter(intervention ->
            intervention.hasSpeakerThatContains("Tamames")).collect(Collectors.toList()));
        assertThat(interventionWithOneAuthorCalledTamames.size()).isEqualTo(8);
        for (Intervention intervention:interventionWithOneAuthorCalledTamames) {
            System.out.println(intervention);
        }
    }
}
