package org.ecos.logic.deputies.study.deputies;

import org.ecos.logic.deputies.study.CommonResourceFunctionality;
import org.ecos.logic.deputies.study.deputy.Deputy;
import org.ecos.logic.deputies.study.deputy.parse.json.DeputyParser;
import org.ecos.logic.deputies.study.object_mother.CircumscriptionObjectMother;
import org.ecos.logic.deputies.study.policitalparty.ParliamentaryGroup;
import org.json.JSONArray;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.ecos.logic.deputies.study.data.entity.CommonConstant.LEGISLATURE_XIV_ID;

class DeputiesFromJsonTest {
    @Test
    void getAllDeputies() throws IOException {
        String result = CommonResourceFunctionality.readFromRecourcesFileToString("/deputies/deputies.json");
        JSONArray jsonArray = new JSONArray(result);

        List<Deputy> deputyList = DeputyParser.parseList(jsonArray);

        assertThat(deputyList.size()).isEqualTo(348);

        Deputy deputy = deputyList.get(1);
        assertThat(deputy).extracting(
                Deputy::getIdentity,
                Deputy::getName,
                Deputy::getSurname,
                Deputy::getSurnameName,
                Deputy::getPoliticalParty,
                Deputy::getDetailedPoliticalParty,
                Deputy::getStart,
                Deputy::getEnd,
                Deputy::getGenre,
                Deputy::getLocationIdentity,
                Deputy::getLocationName,
                Deputy::getLegislatureIdentity
        ).containsExactly(
                43,
                "Santiago",
                "Abascal Conde",
                "Abascal Conde, Santiago",
                "Vox",
                "Grupo Parlamentario VOX",
                "20/11/2019",
                "",
                "1",
                28,
                "Madrid",
                14
        );
    }

    @Test
    void getAllPoliticalParties() throws IOException {
        String result = CommonResourceFunctionality.readFromRecourcesFileToString("/deputies/deputies.json");

        List<Deputy> deputyList = DeputyParser.parseList(new JSONArray(result));

        Set<String> politicalParties = deputyList.stream().map(Deputy::getPoliticalParty).collect(Collectors.toSet());

        assertThat(politicalParties.size()).isEqualTo(25);
        System.out.println(politicalParties);
        List<StringBuilder> insertBuilder = new ArrayList<>();
        int i = 1;
        for (String politicalParty : politicalParties) {
            StringBuilder builder = new StringBuilder();
            builder.append("INSERT INTO \n \tpolitical_party\n(\n\tpolitical_party_id,\n\tshort_name,\n\tname\n)\nVALUES\n(\n\t");
            builder.append(i);
            builder.append(",\n\t'");
            builder.append(politicalParty);
            builder.append("',\n\t'");
            builder.append(politicalParty);
            builder.append("'\n);");
            insertBuilder.add(builder);
            i++;
        }
        for (StringBuilder builder : insertBuilder) {
            System.out.println(builder.toString());
        }
    }

    @Test
    void getAllCircumscriptions() throws IOException {
        String result = CommonResourceFunctionality.readFromRecourcesFileToString("/deputies/deputies.json");

        List<Deputy> deputyList = DeputyParser.parseList(new JSONArray(result));

        Set<String> circumscriptionList = deputyList.stream().map(Deputy::getLocationName).collect(Collectors.toSet());

        assertThat(circumscriptionList.size()).isEqualTo(52);
    }

    @Test
    void prepareToCreateDeputiesData() throws IOException {
        Map<String, Integer> circumscriptionMap = CircumscriptionObjectMother.generateCollection();
        String result = CommonResourceFunctionality.readFromRecourcesFileToString("/deputies/deputies.json");
        JSONArray jsonArray = new JSONArray(result);

        List<Deputy> deputyList = DeputyParser.parseList(jsonArray);

        assertThat(deputyList.size()).isEqualTo(348);

        List<org.ecos.logic.deputies.study.policitalparty.Deputy> deputiesToInsert = deputyList.stream().map(deputy ->{

                int circumscriptionId = circumscriptionMap.get(deputy.getLocationName());
                return new org.ecos.logic.deputies.study.policitalparty.Deputy(
                    circumscriptionId,
                    deputy.getName(),
                    deputy.getSurname(),
                    deputy.getGenre().equals("1"),
                    LEGISLATURE_XIV_ID);
            }
        ).collect(Collectors.toList());
        List<StringBuilder> insertBuilder = new ArrayList<>();
        for (org.ecos.logic.deputies.study.policitalparty.Deputy deputy : deputiesToInsert) {
            StringBuilder builder = new StringBuilder();
            builder.append("INSERT INTO\n \t" +
                    "deputy\n" +
                    "(\n\t" +
                    "circumscription_id,\n\t" +
                    "name,\n\t" +
                    "surname,\n\t" +
                    "is_male,\n\t" +
                    "deputy_legislature_code\n" +
                    ")\nVALUES\n" +
                    "(\n\t");
            builder.append("'");
            builder.append(deputy.getCircumscriptionId());
            builder.append("',\n\t'");
            builder.append(deputy.getName());
            builder.append("',\n\t'");
            builder.append(deputy.getSurname());
            builder.append("',\n\t'");
            builder.append(deputy.isMale()?"1":"0");
            builder.append("',\n\t'");
            builder.append(deputy.getLegislatureId());
            builder.append("'\n);");
            insertBuilder.add(builder);
        }
        for (StringBuilder builder : insertBuilder) {
            System.out.println(builder.toString());
        }
    }


    @Test
    void getAllPoliticalGroupByLegislature() throws IOException {
        String result = CommonResourceFunctionality.readFromRecourcesFileToString("/deputies/deputies.json");
        JSONArray jsonArray = new JSONArray(result);

        List<Deputy> deputyList = DeputyParser.parseList(jsonArray);

        Set<ParliamentaryGroup> parliamentaryGroup = deputyList.stream().map(deputy -> new ParliamentaryGroup(deputy.getDetailedPoliticalParty())).collect(Collectors.toSet());
        assertThat(parliamentaryGroup.size()).isEqualTo(10);
        int i = 1;
        List<StringBuilder> insertBuilder = new ArrayList<>();
        for (ParliamentaryGroup politicalParty : parliamentaryGroup) {
            StringBuilder builder = new StringBuilder();
            builder.append("INSERT INTO \n \t" +
                    "parliamentary_group\n" +
                    "(\n\t" +
                    "parliamentary_group_id,\n\t" +
                    "group_name\n" +
                    ")\n" +
                    "VALUES\n" +
                    "(\n\t");
            builder.append(i);
            builder.append(",\n\t'");
            builder.append(politicalParty.getPoliticalGroupName());
            builder.append("'\n);");
            insertBuilder.add(builder);
            i++;
        }
        for (StringBuilder builder : insertBuilder) {
            System.out.println(builder.toString());
        }
    }
}
