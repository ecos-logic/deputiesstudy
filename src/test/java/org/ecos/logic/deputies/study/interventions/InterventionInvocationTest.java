package org.ecos.logic.deputies.study.interventions;

import org.ecos.logic.deputies.study.CommonResourceFunctionality;
import org.ecos.logic.deputies.study.intervention.Intervention;
import org.ecos.logic.deputies.study.rest.client.InterventionInvocation;
import org.json.JSONArray;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

class InterventionInvocationTest {

    private static final int NUMBER_OF_INITIATIVES_PER_INVOCATION = 25;
    private final InterventionInvocation interventionInvocation = new InterventionInvocation();

    @Test
    void getAllInterventionCounter(){
        int initiativeTotal = this.interventionInvocation.getInterventionsCounter();
        assertThat(initiativeTotal).isEqualTo(81907);
    }

    @Test
    void getAllInterventions() throws IOException {
        int interventionsTotal = this.interventionInvocation.getInterventionsCounter();

        int counter = interventionsTotal / NUMBER_OF_INITIATIVES_PER_INVOCATION;
        if (interventionsTotal % NUMBER_OF_INITIATIVES_PER_INVOCATION > 0)
            counter++;

        List<Intervention> result = interventionInvocation.getAllInterventions(counter);

        JSONArray jsonArray = new JSONArray(result);
        CommonResourceFunctionality.writeTo("/interventions/allInterventions.json", jsonArray.toString(4));

        assertThat(result).hasSize(interventionsTotal);

    }
}
