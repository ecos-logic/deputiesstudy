package org.ecos.logic.deputies.study.object_mother;

import java.util.HashMap;
import java.util.Map;

public class CircumscriptionObjectMother {
    public static Map<String, Integer> generateCollection(){
        Map<String,Integer> circumscriptionMap = new HashMap<>();
        circumscriptionMap.put("Araba/Álava",1);
        circumscriptionMap.put("Albacete",2);
        circumscriptionMap.put("Alicante/Alacant",3);
        circumscriptionMap.put("Almería",4);
        circumscriptionMap.put("Ávila",5);
        circumscriptionMap.put("Badajoz",6);
        circumscriptionMap.put("Balears (Illes)",7);
        circumscriptionMap.put("Barcelona",8);
        circumscriptionMap.put("Burgos",9);
        circumscriptionMap.put("Cáceres",10);
        circumscriptionMap.put("Cádiz",11);
        circumscriptionMap.put("Castellón/Castelló",12);
        circumscriptionMap.put("Ciudad Real",13);
        circumscriptionMap.put("Córdoba",14);
        circumscriptionMap.put("Coruña (A)",15);
        circumscriptionMap.put("Cuenca",16);
        circumscriptionMap.put("Girona",17);
        circumscriptionMap.put("Granada",18);
        circumscriptionMap.put("Guadalajara",19);
        circumscriptionMap.put("Gipuzkoa",20);
        circumscriptionMap.put("Huelva",21);
        circumscriptionMap.put("Huesca",22);
        circumscriptionMap.put("Jaén",23);
        circumscriptionMap.put("León",24);
        circumscriptionMap.put("Lleida",25);
        circumscriptionMap.put("Rioja (La)",26);
        circumscriptionMap.put("Lugo",27);
        circumscriptionMap.put("Madrid",28);
        circumscriptionMap.put("Málaga",29);
        circumscriptionMap.put("Murcia",30);
        circumscriptionMap.put("Navarra",31);
        circumscriptionMap.put("Ourense",32);
        circumscriptionMap.put("Asturias",33);
        circumscriptionMap.put("Palencia",34);
        circumscriptionMap.put("Palmas (Las)",35);
        circumscriptionMap.put("Pontevedra",36);
        circumscriptionMap.put("Salamanca",37);
        circumscriptionMap.put("S/C Tenerife",38);
        circumscriptionMap.put("Cantabria",39);
        circumscriptionMap.put("Segovia",40);
        circumscriptionMap.put("Sevilla",41);
        circumscriptionMap.put("Soria",42);
        circumscriptionMap.put("Tarragona",43);
        circumscriptionMap.put("Teruel",44);
        circumscriptionMap.put("Toledo",45);
        circumscriptionMap.put("Valencia/València",46);
        circumscriptionMap.put("Valladolid",47);
        circumscriptionMap.put("Bizkaia",48);
        circumscriptionMap.put("Zamora",49);
        circumscriptionMap.put("Zaragoza",50);
        circumscriptionMap.put("Ceuta",51);
        circumscriptionMap.put("Melilla",52);

        return circumscriptionMap;
    }
}
