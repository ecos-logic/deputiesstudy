package org.ecos.logic.deputies.study.initiatives;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.ecos.logic.deputies.study.initiative.Initiative;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.ecos.logic.deputies.study.CommonResourceFunctionality.readFromRecourcesFileToString;
import static org.ecos.logic.deputies.study.CommonResourceFunctionality.writeTo;

class InitiativesExtractingTest {
    public static final int INITIATIVES_COUNTER = 140589;

    @Test
    void getAllInitiativesFromAllInitiativesResult() throws IOException {
        List<Initiative> initiatives = getInitiativesFromAllInitiativesJson();

        assertThat(initiatives.size()).isEqualTo(INITIATIVES_COUNTER);

        String deputyInitiatives = readFromRecourcesFileToString("/deputies/deputiesClear");


        List<String> deputies = Arrays.stream(deputyInitiatives.split("\n")).collect(Collectors.toList());

        StringBuilder titlesPerAuthorBuilder = new StringBuilder();
        for (String deputy : deputies) {

            StringBuilder titlesBuilder = new StringBuilder();
            int titlesCounter = 0;
            for (String title : new HashSet<>(initiatives.stream().filter(i -> i.getAuthor().contains(deputy)).map(Initiative::getTitle).sorted().collect(Collectors.toList())).stream().sorted().collect(Collectors.toList())) {
                titlesBuilder.append(String.format("%s\n", title));
                titlesCounter++;
            }

            titlesPerAuthorBuilder.append(
                String.format(
                    "-- --------------------------------------------------------------------------------- --\n" +
                    "\t\t%s (%d)\n" +
                    "-- --------------------------------------------------------------------------------- --\n\n" +
                    "%s\n\n",
                    deputy,
                    titlesCounter,
                    titlesBuilder
                )
            );
        }

        writeTo("/initiatives/titlesPerDeputy", titlesPerAuthorBuilder.toString());
    }

    @Test
    void countingInitiativeWords() throws IOException {
        String deputyInitiatives = readFromRecourcesFileToString("/initiatives/deputyInitiatives");

        // Convert the text to lowercase to avoid counting words with different capitalization as different words
        deputyInitiatives = deputyInitiatives.toLowerCase();

        // Split the text into words using a word separator that includes spaces, punctuation marks, and other non-alphabetic characters
        String[] words = deputyInitiatives.split("[^\\p{IsAlphabetic}0-9']+");

        // Create a HashMap to store the frequency of each word
        Map<String, Integer> wordFrequency = new HashMap<>();

        // Iterate over each word and increase its frequency in the HashMap
        for (String word : words) {
            if (!Arrays.asList(
                    "su", "s", "n", "de", "la", "del", "a", "el", "y", "en", "los",
                    "las", "que", "as", "así", "como", "por", "al", "sobre", "un",
                    "una", "entre", "con", "0000", "184", "o", "acerca", "para", "c",
                    "se", "digo", "mero", "p", "han", "e", "desde", "incluidas",
                    "incluidos", "sus", "si", "no"
            ).contains(word)) {
                Integer frequency = wordFrequency.get(word);
                wordFrequency.put(word, frequency == null ? 1 : frequency + 1);
            }
        }

        // Convert the HashMap into a list and sort it by frequency in descending order
        List<Map.Entry<String, Integer>> wordList = new ArrayList<>(wordFrequency.entrySet());
        wordList.sort(Map.Entry.<String, Integer>comparingByValue().reversed());

        // Print the top 25 most frequent words
        int counter = 0;
        for (Map.Entry<String, Integer> entry : wordList) {
            if (counter == 50) {
                break;
            }
            System.out.println(entry.getKey() + " : " + entry.getValue());
            counter++;
        }
    }

    @Test
    void get50DeputiesWordsCounter() throws IOException {
        String deputies = readFromRecourcesFileToString("/deputies/deputiesClear");
        List<Initiative> initiatives = getInitiativesFromAllInitiativesJson();

        StringBuilder builderString = new StringBuilder();
        for (String deputy : deputies.split("\n")) {
            StringBuilder titlesBuilder = new StringBuilder();

            for (String title : new HashSet<>(initiatives.stream().filter(i -> i.getAuthor().contains(deputy)).map(Initiative::getTitle).sorted().collect(Collectors.toList())).stream().sorted().collect(Collectors.toList())) {
                titlesBuilder.append(title);
            }

            String deputyInitiatives = titlesBuilder.toString().toLowerCase();


            String[] words = deputyInitiatives.split("[^\\p{IsAlphabetic}0-9']+");


            Map<String, Integer> wordFrequency = new HashMap<>();


            for (String word : words) {
                if (!Arrays.asList(
                        "su", "s", "n", "de", "la", "del", "a", "el", "y", "en", "los",
                        "las", "que", "as", "así", "como", "por", "al", "sobre", "un",
                        "una", "entre", "con", "0000", "184", "o", "acerca", "para", "c",
                        "se", "digo", "mero", "p", "han", "e", "desde", "incluidas",
                        "incluidos", "sus", "si", "no", "ha", "lo", "tiene", "i", "les",
                        "els", "qué", "sea", "u", "es", "l", "r", "cuál", "ante", "cada",
                        "231", "950", "2020", "2021", "2022", "2010", "19", "2017", "01", "25",
                        "06", "2025", "31", "04", "463", "2013", "1", "2", "3", "4", "5", "6", "7",
                        "8", "9"

                ).contains(word)) {
                    Integer frequency = wordFrequency.get(word);
                    wordFrequency.put(word, frequency == null ? 1 : frequency + 1);
                }
            }


            List<Map.Entry<String, Integer>> wordList = new ArrayList<>(wordFrequency.entrySet());
            wordList.sort(Map.Entry.<String, Integer>comparingByValue().reversed());


            int counter = 0;
            for (Map.Entry<String, Integer> entry : wordList) {
                if (counter == 50) {
                    break;
                }

                if (entry.getKey().equals("eta")) {
                    builderString.append(String.format("%s: %s : %d\n", deputy, entry.getKey(), entry.getValue()));
                    System.out.printf("%s: %s : %d%n", deputy, entry.getKey(), entry.getValue());

                    counter++;
                }
            }
        }

        writeTo("/initiatives/word50WordCounterPerDeputy", builderString.toString());
    }

    private static List<Initiative> getInitiativesFromAllInitiativesJson() throws IOException {
        String result = readFromRecourcesFileToString("/initiatives/allInitiatives.json");
        Gson gson = new Gson();
        Type listOfMyClassObject = new TypeToken<ArrayList<Initiative>>() {}.getType();
        return gson.fromJson(result, listOfMyClassObject);
    }
}