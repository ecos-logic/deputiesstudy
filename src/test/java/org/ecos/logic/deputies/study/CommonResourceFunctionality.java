package org.ecos.logic.deputies.study;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;

public class CommonResourceFunctionality {
    public static String readFromRecourcesFileToString(@SuppressWarnings("SameParameterValue") String name) throws IOException {
        URL filepath = CommonResourceFunctionality.class.getResource(name);
        if (filepath == null)
            throw new FileNotFoundException();

        return FileUtils.readFileToString(new File(filepath.getFile()), "UTF-8");
    }

    public static void writeTo(String relativePathToResources, String content) throws IOException {
        URL filepath = CommonResourceFunctionality.class.getResource(relativePathToResources);
        if (filepath == null)
            throw new FileNotFoundException();

        String pathname = filepath.getFile();
        File file = new File(pathname);
        FileUtils.writeStringToFile(file, content, Charset.defaultCharset());
    }
}
