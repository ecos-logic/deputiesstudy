package org.ecos.logic.deputies.study.interventions;

import org.ecos.logic.deputies.study.CommonResourceFunctionality;
import org.ecos.logic.deputies.study.intervention.detail.Author;
import org.ecos.logic.deputies.study.intervention.detail.Detail;
import org.ecos.logic.deputies.study.intervention.detail.Link;
import org.ecos.logic.deputies.study.intervention.session.Session;
import org.ecos.logic.deputies.study.intervention.session.Video;
import org.ecos.logic.deputies.study.intervention.Intervention;
import org.ecos.logic.deputies.study.intervention.json.InterventionParser;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Java6Assertions.assertThat;

class InterventionParseTest {
    @Test
    void parseTest() throws IOException {
        String result = CommonResourceFunctionality.readFromRecourcesFileToString("/interventions/20230426_1813/result669.json");

        JSONObject jsonObject = new JSONObject(result);
        List<Intervention> interventionList = InterventionParser.toInterventionCollection(jsonObject);

        assertThat(interventionList).hasSize(25);

        Intervention intervention = interventionList.stream().filter(in -> in.getDocumentCode().equals("16720")).findAny().orElse(null);
        assertThat(intervention).isNotNull();

        assert intervention != null;
        assertThat(intervention).
            extracting(
                Intervention::getDateAsString,
                Intervention::getLegislatureCode,
                Intervention::getDocumentCode,
                Intervention::getSpeakerCharge,
                Intervention::getSpeaker,
                Intervention::getDocumentLink,
                Intervention::getDocumentOtherLink,
                Intervention::getDocumentFormat
            ).
            containsExactly(
        "20200626",
                "XIV",
                "16720",
                "Administradora provisional única de la Corporación RTVE",
                "Mateo Isasi, Rosa María",
                "(DSCG-14-CM-15.CODI.)#(Página10)",
                "CORT/DS/CM/DSCG-14-CM-15.PDF#page=10",
                "PUWTZDTS.fmt"
            );
        assertThat(intervention.getLink()).
            extracting(
                org.ecos.logic.deputies.study.intervention.Link::getPiece,
                org.ecos.logic.deputies.study.intervention.Link::getDocumentReference,
                org.ecos.logic.deputies.study.intervention.Link::getFormat,
                org.ecos.logic.deputies.study.intervention.Link::getQuery,
                org.ecos.logic.deputies.study.intervention.Link::getCommand,
                org.ecos.logic.deputies.study.intervention.Link::getUrl,
                org.ecos.logic.deputies.study.intervention.Link::getBase
            ).
            containsExactly(
                "ITA4",
                "16720",
                "INTTZD1S.fmt",
                "(XIV.LEGI.).",
                "VERDOC",
                "/wc/servidorCGI",
                "IT14"
            );
        assertThat(intervention.getDetail()).
            extracting(
                Detail::getSource,
                Detail::getIdentity,
                Detail::getTitle
            ).
            containsExactly(
                "358",
                "178 C 178/000059",
                "¿Qué valoración hace la administradora única provisional del modelo de financiación de RTVE sin publicidad?"
            );
        assertThat(intervention.getDetail().getLink()).
            extracting(
                Link::getDocumentReference,
                Link::getPiece,
                Link::getFormat,
                Link::getCommand,
                Link::getInitiativeIdentity,
                Link::getUrl,
                Link::getBase
            ).
            containsExactly(
                "2020062600035800500014336n00551780000590000654192",
                "ITA4",
                "INITZLSS.fmt",
                "VERLST",
                "178/000059/0000",
                "/wc/servidorCGI",
                "IT14"
            );

        assertThat(intervention.getDetail().getAuthorList()).hasSize(2);
        assertThat(intervention.getDetail().getAuthorList().stream().map(Author::getName).collect(Collectors.toList())).contains(
                "Mariscal Zabala, Manuel (GVOX)",
                "<br>Sánchez del Real, Víctor Manuel (GVOX)"
        );

        assertThat(intervention.getDetail().getAuthorList().get(0)).
                extracting(
                    Author::getIdentity,
                    Author::getOrder,
                    Author::getLegislatureCode,
                    Author::getLink
                ).containsExactly(
            153,
                    1,
                    "ITA4",
                    "/wc/fichaDiputado"
                );

        assertThat(intervention.getSession()).
            extracting(
                Session::getName,
                Session::getIdentity,
                Session::getPhase
            ).
            containsExactly(
        "Comisión Mixta Control Parlam. de la Corporación RTVE y sus Sociedades",
                "5",
                "Pregunta-contestación"
            );
        assertThat(intervention.getSession().getVideoLlList().get(0)).
            extracting(
                    Video::getTimeCodeAsString,
                    Video::getLegislature,
                    Video::getUrl,
                    Video::getLinkToDownload,
                    Video::getLinkToBroadcast,
                    Video::getSequence,
                    Video::getStartCode,
                    Video::getIdentity,
                    Video::getSource
            ).
            containsExactly(
                "13:04:39",
                "14",
                "14_000358_005_0_20090_654192.wmv",
                "https://static.congreso.es/audiovisual/video/leg14/358/14_000358_005/cortes/14_000358_005_0_20090_654188.mp4",
                "https://app.congreso.es/v1/14654188I",
                "5",
                "125912",
                "654188",
                "358"
            );

    }

}
