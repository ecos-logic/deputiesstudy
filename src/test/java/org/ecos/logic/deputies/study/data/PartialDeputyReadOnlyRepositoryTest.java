package org.ecos.logic.deputies.study.data;

import org.ecos.logic.deputies.study.CommonResourceFunctionality;
import org.ecos.logic.deputies.study.data.entity.PartialDeputyReadOnly;
import org.ecos.logic.deputies.study.data.repository.DeputyRepository;
import org.ecos.logic.deputies.study.deputy.Deputy;
import org.ecos.logic.deputies.study.deputy.parse.json.DeputyParser;
import org.json.JSONArray;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.*;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.ecos.logic.deputies.study.object_mother.PartialDeputyReadOnlyObjectMother.getCollection;
import static org.ecos.logic.deputies.study.object_mother.PoliticalPartyObjectMother.getNameAndIdCollection;

@SpringBootTest(classes = DataApplication.class)
class PartialDeputyReadOnlyRepositoryTest {
    @Autowired
    private DeputyRepository repository;

    @Test
    void buildPoliticalPartyRelDeputy() throws IOException {
        Map<String, Integer> politicalPartyNameAndId = getNameAndIdCollection();

        List<PartialDeputyReadOnly> partialDeputyReadOnlyList = repository.findAll();
        assertThat(partialDeputyReadOnlyList.size()).isEqualTo(348);


        Map<PartialDeputyReadOnly, Integer> partialDeputyReadOnlyMap = getCollection();
        for (PartialDeputyReadOnly partialDeputy : partialDeputyReadOnlyMap.keySet()) {
            PartialDeputyReadOnly deputyFromDatabase = partialDeputyReadOnlyList.stream().filter(partialDeputyReadOnly -> partialDeputyReadOnly.equals(partialDeputy)).findFirst().orElseThrow();
            partialDeputyReadOnlyMap.put(deputyFromDatabase, deputyFromDatabase.getId());
            partialDeputy.setId(deputyFromDatabase.getId());
        }

        assertThat(partialDeputyReadOnlyMap.keySet().size()).isEqualTo(348);
        assertThat(partialDeputyReadOnlyMap.values().size()).isEqualTo(348);

        String result = CommonResourceFunctionality.readFromRecourcesFileToString("/deputies/deputies.json");
        JSONArray jsonArray = new JSONArray(result);

        List<Deputy> deputyList = DeputyParser.parseList(jsonArray);

        List<StringBuilder> insertBuilder = new ArrayList<>();
        for (Deputy deputy : deputyList) {
            StringBuilder builder = new StringBuilder();
            builder.append("INSERT INTO \n \t" +
                    "political_party_rel_deputy\n" +
                    "(\n\t" +
                    "deputy_id,\n\t" +
                    "political_party_id\n" +
                    ")\n" +
                    "VALUES\n" +
                    "(\n\t");
            PartialDeputyReadOnly lerele = new PartialDeputyReadOnly(deputy.getName(), deputy.getSurname(), deputy.getGenre().equals("2") ? "0" : "1");
//            System.out.println(lerele);
            builder.append(partialDeputyReadOnlyMap.get(lerele));
            builder.append(",\n\t");
            builder.append(politicalPartyNameAndId.get(deputy.getPoliticalParty()));
            builder.append("\n);");
            insertBuilder.add(builder);
        }

        for (StringBuilder builder : insertBuilder) {
            System.out.println(builder.toString());
        }

    }
}
