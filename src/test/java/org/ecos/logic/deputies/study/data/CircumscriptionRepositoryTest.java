package org.ecos.logic.deputies.study.data;

import org.ecos.logic.deputies.study.data.entity.Circumscription;
import org.ecos.logic.deputies.study.data.repository.CircumscriptionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

@SpringBootTest(classes = DataApplication.class)
class CircumscriptionRepositoryTest {
    @Autowired
    private CircumscriptionRepository repository;
    @Test
    void getAllCircumscription(){
        List<Circumscription> circumscriptionList = repository.findAll();
        assertThat(circumscriptionList.size()).isEqualTo(52);
    }
}
