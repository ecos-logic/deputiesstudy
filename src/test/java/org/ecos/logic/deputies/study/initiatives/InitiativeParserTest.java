package org.ecos.logic.deputies.study.initiatives;

import org.ecos.logic.deputies.study.App;
import org.ecos.logic.deputies.study.initiative.Initiative;
import org.ecos.logic.deputies.study.initiative.Link;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.ecos.logic.deputies.study.initiative.parser.json.InitiativeParser.toInitiativeCollection;

class InitiativeParserTest {
    @Test
    void canIParseAGivenInitiativeResultAnGetTheListOfInitiatives() throws URISyntaxException, FileNotFoundException, ClassNotFoundException {
        URL filepath = App.class.getResource("/initiatives/20230402_2000/result20.json");
        if (filepath == null)
            throw new FileNotFoundException();

        URI uri = filepath.toURI();

        JSONTokener parser = new JSONTokener(new FileReader(uri.getPath()));
        JSONObject jsonObject = new JSONObject(parser);

        List<Initiative> initiatives = new ArrayList<>(toInitiativeCollection(jsonObject));

        assertThat(initiatives).
                hasSize(25);

        Optional<Initiative> anOptionalInitiative = initiatives.stream().
                filter(e ->
                        e.getTitle().equals("Proposición de Ley de medidas urgentes para hacer frente a la ocupación ilegal de inmuebles.") &&
                                e.getDocument().equals("480")
                ).
                findFirst();

        assertThat(anOptionalInitiative.isPresent()).isTrue();

        anOptionalInitiative = initiatives.stream().
                filter(e ->
                        e.getTitle().equals("Proposición de Ley sobre concesión de nacionalidad española a los saharauis nacidos bajo la soberanía española.") &&
                                e.getDocument().equals("496")
                ).
                findFirst();

        assertThat(anOptionalInitiative.isPresent()).isTrue();

        anOptionalInitiative = initiatives.stream().
                filter(e ->
                        e.getDocument().equals("497")
                ).
                findFirst();

        assertThat(anOptionalInitiative.isPresent()).isTrue();
        if (anOptionalInitiative.isEmpty())
            throw new ClassNotFoundException();

        Initiative aGivenInitiative = anOptionalInitiative.get();

        assertThat(aGivenInitiative.getDocument()).isEqualTo("497");
        assertThat(aGivenInitiative.getLegislature()).isEqualTo("XIV");
        assertThat(aGivenInitiative.getTitle()).isEqualTo("Proposición de Ley Orgánica por la que se modifica la Ley Orgánica 10/1995, de 23 de noviembre, del Código Penal, para agravar las penas previstas para los delitos de trata de seres humanos desplazados por un conflicto armado o una catástrofe humanitaria.");
        assertThat(aGivenInitiative.getResult()).isEqualTo("Aprobado con modificaciones");
        Link aGivenLink = aGivenInitiative.getLink();
        assertThat(aGivenLink).
                hasFieldOrPropertyWithValue("documents", "497-497").
                hasFieldOrPropertyWithValue("piece", "IWA4").
                hasFieldOrPropertyWithValue("query", "I.ACIN1.").
                hasFieldOrPropertyWithValue("command", "VERLST").
                hasFieldOrPropertyWithValue("fmt", "INITZD1S.fmt").
                hasFieldOrPropertyWithValue("formulary", "INITZLUS.fmt").
                hasFieldOrPropertyWithValue("url", "/wc/servidorCGI").
                hasFieldOrPropertyWithValue("base", "IW14");

        assertThat(aGivenInitiative.getQualifiedDate()).isEqualTo("19/04/2022");
        assertThat(aGivenInitiative.getPresentedDate()).isEqualTo("06/04/2022");
        assertThat(aGivenInitiative.getIdentity()).isEqualTo("122/000214");
        assertThat(aGivenInitiative.getAuthor()).isEqualTo("Grupo Parlamentario Socialista");
    }

}
